function startEvent (type, questing) {
	eventType = type;
	isQuest = questing;

	keysEnabled = 0;
	eventEl.style.display = "block";
	screen.style.display = "none";
	screen.innerHTML = "";

	eventHeaderEl.style.lineHeight = eventHeaderEl.offsetHeight + "px";
	eventPlayEl.style.visibility = "visible";
	eventPlayEl.onclick = function() { initEvent(); };
	eventLeaveEl.innerHTML = eventRun;
	eventLeaveEl.onclick = function() { endEvent(-1); };

	var introMessage = "";
	switch (eventType) {
		case 1: introMessage = introMessageCatch[randomInt(0, introMessageCatch.length - 1)]; break;
		case 2: introMessage = introMessageMath[randomInt(0, introMessageMath.length - 1)]; break;
		case 3: introMessage = introMessageFight[randomInt(0, introMessageFight.length - 1)]; break;
		case 4: introMessage = introMessagePattern[randomInt(0, introMessagePattern.length - 1)]; break;
		default: introMessage = eventIntroGeneric; break;
	}
	if (isQuest) {
		if (towns[questTown][5] > 5) introMessage = randomQuests[1];
		else {
			switch (questTown) {
				case 1: introMessage = questsQuick[towns[questTown][5]][1]; break;
				case 2: introMessage = questsSmart[towns[questTown][5]][1]; break;
				case 0: introMessage = questsTough[towns[questTown][5]][1]; break;
			}
		}
	}

	eventMsgEl.innerHTML = introMessage;
}

function initEvent () {
	eventPlayEl.style.visibility = "hidden";
	screen.style.display = "block";
	screen.innerHTML = "";

	eventScreenSizeX = screen.offsetWidth;
	eventScreenSizeY = screen.offsetHeight;

	switch (eventType) {
		case 1: catchRunners(); break;
		case 2: solveMath(); break;
		case 3: clickFight(); break;
		case 4: solvePattern(); break;
	}
}

function endEvent (win) {
	var reward;
	var rewardType;
	var eventMsg = "";

	if (catchInterval) clearInterval(catchInterval);
	if (fightInterval) clearInterval(fightInterval);

	if (isQuest) {
		isQuest = 0;
		if (win == 1) {
			questDone = 1;
			map[position[0]][position[1]][0] = questSavedCell;
			map[towns[questTown][0]][towns[questTown][1]][0] = quest;
			if (towns[questTown][5] > 5) addToLog(randomQuests[2]);
			else {
				switch (questTown) {
				case 1: addToLog(questsQuick[towns[questTown][5]][2]); break;
				case 2: addToLog(questsSmart[towns[questTown][5]][2]); break;
				case 0: addToLog(questsTough[towns[questTown][5]][2]); break;
				}
			}
			displayMap();
		}
	}

	if (win == 1) {
		eventLeaveEl.innerHTML = eventLeave;
		eventLeaveEl.onclick = function() { leaveEvent(); };
		switch (randomInt(1, 6)) {
			case 1:
				rewardType = symbolFood;
				reward = randomInt(1, level);
				food += reward;
				break;
			case 2:
			case 3:
				rewardType = symbolWood;
				reward = randomInt(1, level * level);
				wood += reward;
				break;
			default:
				rewardType = symbolCoin;
				reward = randomInt(1, level * level * level);
				coinChange(reward);
				break;
		}
		eventMsg = eventWinResult + dozenal(reward) + rewardType + "!";
		switch(eventType) {
			case 1:
				incAttribute(1);
				noEventsWonCatch++;
				break;
			case 2:
				incAttribute(2);
				noEventsWonMath++;
				break;
			case 3:
				incAttribute(3);
				noEventsWonFight++;
				break;
			case 4:
				incAttribute(randomInt(1, 3));
				noEventsWonPattern++;
				break;
			default:
				incAttribute(randomInt(1, 3));
				break;
		}
	} else if (win == -1) {
		reward = Math.floor(coin / 12);
		coinChange(-reward);
		eventMsg = eventRunResult + dozenal(reward) + symbolCoin;
		leaveEvent();
	} else {
		switch(eventType) {
			case 1: noEventsLostCatch++; break;
			case 2: noEventsLostMath++; break;
			case 3: noEventsLostFight++; break;
			case 4: noEventsLostPattern++; break;
		}
		eventMsg = eventFailed;
		eventLeaveEl.innerHTML = eventLeave;
		eventLeaveEl.onclick = function() { leaveEvent(); };
	}
	eventMsgEl.innerHTML = eventMsg;
	addToLog(eventMsg);
}

function leaveEvent () {
	keysEnabled = 1;
	eventEl.style.display = "none";
	updateButtons();
}

function questPlace (ring) {
	var x, y;
	var randCoord = randomInt(mapCenter - ring, mapCenter + ring);

	switch (randomInt(1, 4)) {
		case 1: x = mapCenter + ring; y = randCoord; break;
		case 2: x = mapCenter - ring; y = randCoord; break;
		case 3: y = mapCenter + ring; x = randCoord; break;
		case 4: y = mapCenter - ring; x = randCoord; break;
	}

	questSavedCell = map[x][y][0];
	map[x][y][0] = quest;
	map[x][y][1] = 1;
}

function questToggle () {
	if (onQuest) {
		onQuest = 0;
		questTown = 99;
		addToLog(questQuit);
		return;
	}

	onQuest = 1;
	questTown = currentTown;
	var townQuestsCompleted = towns[currentTown][5];

	if (townQuestsCompleted > 5) {
		questType = randomInt(1, 4);
		questPlace(randomInt(1, 12));
		addToLog(randomQuests[0]);
	} else {
		switch (questTown) {
			case 0: questType = 3; addToLog(questsTough[townQuestsCompleted][0]); break;
			case 1: questType = 1; addToLog(questsQuick[townQuestsCompleted][0]); break;
			case 2: questType = 2; addToLog(questsSmart[townQuestsCompleted][0]); break;
		}

		switch (townQuestsCompleted) {
			case 0:
				questPlace(1);
				break;
			case 1:
				if (questTown == 0) questType = 4;
				questPlace(randomInt(2, 3));
				break;
			case 2:
				if (questTown == 1) questType = 4;
				questPlace(randomInt(2, 4));
				break;
			case 3:
				if (questTown == 2) questType = 4;
				questPlace(randomInt(5, 7));
				break;
			case 4:
				questPlace(randomInt(8, 9));
				break;
			case 5:
				questPlace(randomInt(10, 11));
				break;
			default:
				break;
		}
	}

	displayMap();
}

function endQuest () {
	onQuest = 0;
	questDone = 0;
	coinChange(towns[questTown][5] * 12);
	map[position[0]][position[1]][0] = town;
	if (towns[questTown][5] > 5) addToLog(randomQuests[3]);
	else {
		switch (questTown) {
			case 1: addToLog(questsQuick[towns[questTown][5]][3]); break;
			case 2: addToLog(questsSmart[towns[questTown][5]][3]); break;
			case 0: addToLog(questsTough[towns[questTown][5]][3]); break;
		}
	}
	towns[questTown][5]++;
	noQuestsCompleted++;
	questTown = 99;
	update();
}

function cellSpecialPlace (cellToPlace) {
	var ring = randomInt(5, 9);
	var x, y;
	var randCoord = randomInt(mapCenter - ring, mapCenter + ring);

	switch (randomInt(1, 4)) {
		case 1: x = mapCenter + ring; y = randCoord; break;
		case 2: x = mapCenter - ring; y = randCoord; break;
		case 3: y = mapCenter + ring; x = randCoord; break;
		case 4: y = mapCenter - ring; x = randCoord; break;
	}

	if (map[x][y][0] == town) return false;
	if (map[x][y][0] == junking) return false;
	if (map[x][y][0] == florist) return false;
	map[x][y][0] = cellToPlace;

	if (cellToPlace == junking) {
		junkingPosX = x;
		junkingPosY = y;
	} else if (cellToPlace == florist) {
		floristPosX = x;
		floristPosY = y;
	}

	return true;
}

function junkPlace () {
	var ring = noJunkFound + 1;
	if (ring > 11) ring = randomInt(1, 11);

	var x, y;
	var randCoord = randomInt(mapCenter - ring, mapCenter + ring);

	switch (randomInt(1, 4)) {
		case 1: x = mapCenter + ring; y = randCoord; break;
		case 2: x = mapCenter - ring; y = randCoord; break;
		case 3: y = mapCenter + ring; x = randCoord; break;
		case 4: y = mapCenter - ring; x = randCoord; break;
	}

	junkPosX = x;
	junkPosY = y;
}

function junkFound () {
	if (map[junkingPosX][junkingPosY][1]) addToLog(junkingFoundJunk);
	else addToLog(junkingFoundJunkUndiscovered);
	junkCurrent = randomInt(1, junk.length - 1);
	junkPosX = 0;
	junkPosY = 0;
}

function junkingVisited () {
	addToLog(junkingResponses[junkCurrent]);
	if (junkCurrent) {
		junkCurrent = 0;
		coinChange(++noJunkFound * 12);
		junkPlace();
	}
}

function checkForAdjecant (x, y, type) {
	if (map[x + 1][y][0] == type) return true;
	if (map[x - 1][y][0] == type) return true;
	if (map[x][y + 1][0] == type) return true;
	if (map[x][y - 1][0] == type) return true;
	return false;
}

function flowerPlaceRandomCellAround (type) {
	var counter = 0;
	var cellSelected = 0;
	var cellList = [];

	for (var i = 1; i < mapSize - 1; i++) {
		for (var j = 1; j < mapSize - 1; j++) {

			if (type == "river") {
				if (checkForAdjecant(i, j, river) || checkForAdjecant(i, j, bridge)) cellList.push(counter);
			}
			else if (type == "building") {
				if (checkForAdjecant(i, j, home) ||
					checkForAdjecant(i, j, town) ||
					checkForAdjecant(i, j, market) ||
					checkForAdjecant(i, j, casino) ||
					checkForAdjecant(i, j, terminal) ||
					checkForAdjecant(i, j, range) ||
					checkForAdjecant(i, j, library) ||
					checkForAdjecant(i, j, gym))
					cellList.push(counter);
			}
			counter++;
		}
	}

	counter = 0;
	cellSelected = randomInt(0, cellList.length);

	for (var i = 1; i < mapSize - 1; i++) {
		for (var j = 1; j < mapSize - 1; j++) {
			if (counter == cellList[cellSelected]) {
				flowerPosX = i;
				flowerPosY = j;
			}
			counter++;
		}
	}
}

function flowerPlaceRandomCellOfType (type) {
	var counter = 0;
	var cellSelected = 0;
	var cellList = [];

	for (var i = 1; i < mapSize - 1; i++) {
		for (var j = 1; j < mapSize - 1; j++) {
			if (map[i][j][0] == type) cellList.push(counter);
			counter++;
		}
	}

	counter = 0;
	cellSelected = randomInt(0, cellList.length);

	for (var i = 1; i < mapSize - 1; i++) {
		for (var j = 1; j < mapSize - 1; j++) {
			if (counter == cellList[cellSelected]) {
				flowerPosX = i;
				flowerPosY = j;
			}
			counter++;
		}
	}
}

function flowerPlace (flower) {
	var random;

	if (noFlowersFound < 12) {
		random = noFlowersFound;
		addToLog(floristRequests[noFlowersFound]);
	} else {
		random = randomInt(0, 11);
		addToLog(floristRequests[random]);
	}

	if (!flowerPosX) {
		flowerActive = random;

		switch (flowerActive) {
			case 0: // town
				random = randomInt(0, 2);
				flowerPosX = towns[random][0];
				flowerPosY = towns[random][1];
				break;
			case 1: //hills
				flowerPlaceRandomCellOfType(hill);
				break;
			case 2: // north
				flowerPosX = randomInt(1, 4);
				flowerPosY = randomInt(1, 23);
				break;
			case 3: // around buildings
				flowerPlaceRandomCellAround("building");
				break;
			case 4: // forest
				while (!flowerPosX) flowerPlaceRandomCellOfType(forest);
				break;
			case 5: // east
				flowerPosX = randomInt(1, 23);
				flowerPosY = randomInt(20, 23);
				break;
			case 6: // river
				flowerPlaceRandomCellOfType(river);
				break;
			case 7: // west
				flowerPosX = randomInt(1, 23);
				flowerPosY = randomInt(1, 4);
				break;
			case 8: // plains
				while (!flowerPosX) flowerPlaceRandomCellOfType(plain);
				break;
			case 9: // south
				flowerPosX = randomInt(20, 23);
				flowerPosY = randomInt(1, 23);
				break;
			case 10: // around river
				flowerPlaceRandomCellAround("river");
				break;
			case 11: // anywhere
				flowerPosX = randomInt(1, 23);
				flowerPosY = randomInt(1, 23);
				break;
		}
	}
}

function flowerFound () {
	addToLog(floristFoundFlower);
	flowerCurrent = 1;
	flowerPosX = 0;
	flowerPosY = 0;
}

function floristVisited () {
	if (flowerCurrent) {
		addToLog(floristResponse);
		flowerActive = 0;
		flowerCurrent = 0;
		coinChange(++noFlowersFound * 12);
	} else flowerPlace();
}
