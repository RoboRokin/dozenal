var version = "A2"
var testing = 1;
var map = [];
var towns = [];
var mapCenter = 12;
var position = [mapCenter, mapCenter];

// STATS
var rep = 0;
var time = 0;
var energy = 12;
var coin = 0;
var wood = 0;
var food = 0;
var quick = 1;
var smart = 1;
var tough = 1;
var quickProgress = 1;
var smartProgress = 1;
var toughProgress = 1;
var godmode = 0;
var gameOver = 0;
var gameOverTime = 0;
var repRank = 0;
var logSize = 3;
var mapLevel = 1;
var styleLevel = 1;
var radius = 1;
var casinoNet = 0;

// INVENTORY
var gotAxe = 0;
var gotClock = 0;
var gotCompass = 0;
var gotKeyboard = 0;
var gotSeedling = 0;
var junkCurrent = 0;
var flowerCurrent = 0;

// QUESTS
var onQuest = 0;
var questDone = 0;
var questTown = 99;
var questType;
var questSavedCell;

// MILESTONES
var noMoves = 0;
var noEnergyUsed = 0;
var noCoinGot = 0;
var noCoinSpent = 0;
var noWoodChopped = 0;
var noBuildingsErected = 0;
var noQuestsCompleted = 0;
var noJunkFound = 0;
var noFlowersFound = 0;
var noEventsWonCatch = 0;
var noEventsWonMath = 0;
var noEventsWonFight = 0;
var noEventsWonPattern = 0;
var noEventsLostCatch = 0;
var noEventsLostMath = 0;
var noEventsLostFight = 0;
var noEventsLostPattern = 0;

// GAME CONSTS
var milestones = [1, 12, 144, 1728, 20736, 248832, 2985984, 35831808, 429981696,
	5159780352, 61917364224, 743008370688, 8916100448256, 106993205379072,
	1283918464548864, 15407021574586368];
var mapSize = (mapCenter * 2) + 1; // 25
var logSizeMax = 47;
var repRankMax = 7;
var mapLevelMax = 8;
var styleLevelMax = 6;
var energyCostPlain = 1;
var energyCostForest = 2;
var energyCostHill = 4;
var energyCostRiver = 12;
var casinoBet0 = milestones[0];
var casinoBet1 = milestones[1];
var casinoBet2 = milestones[2];
var casinoBet3 = milestones[3];
var casinoBet4 = milestones[4];
var priceAxe = 12;
var priceClock = 144;
var priceCompass = 1728;
var priceKeyboard = 20736;
var priceSeedling = 248832;
var coinGoal = 2985984; // game over

// GAME VARS
var seed;
var cell;
var currentTown;
var cellUpgradeLevel;
var cellUpgradeLevelCost;
var level = 0;
var eventChance = 0;
var repTitle;
var keysEnabled = 0;
var energyRevealed = 0;
var viewSize = 1;
var mapColors = 0;
var mapImages = 0;
var coinAuto = 0;
var priceWood = 12;
var priceWoodBuy = 12;
var priceWoodSell = 12;
var erecting = 0;
var shopping = 0;
var panelToggled = 0;
var eventScreenSizeX = 0;
var eventScreenSizeY = 0;
var eventType;
var isQuest = 0;
var mathNoAnswers;
var mathCorrectAnswer;
var fightInterval;
var fightNoEnemies = 1;
var fightHp = [0, 0, 0, 0, 0];
var fightHpMax = [10, 10, 10, 10, 10];
var catchInterval;
var catchNoCatched;
var catchNoRunners;
var catchRunnerMoving;
var catchRunnersList = [];
var catchRunnersEscapes = [];
var junkPosX;
var junkPosY;
var junkingPosX;
var junkingPosY;
var flowerActive = 0;
var flowerPosX = 0;
var flowerPosY = 0;
var floristPosX;
var floristPosY;

// STYLE
var bgTheme = 0;
var styleLogEntrySize = 12;
var styleLogColumns = 1;

function save () {
	if (window.localStorage) {
		localStorage.seed = seed;
		localStorage.posX = position[0];
		localStorage.posY = position[1];
		localStorage.rep = rep;
		localStorage.time = time;
		localStorage.energy = energy;
		localStorage.coin = coin;
		localStorage.wood = wood;
		localStorage.food = food;
		localStorage.quick = quick;
		localStorage.smart = smart;
		localStorage.tough = tough;
		localStorage.quickProgress = quickProgress;
		localStorage.smartProgress = smartProgress;
		localStorage.toughProgress = toughProgress;
		localStorage.energyRevealed = energyRevealed;
		localStorage.godmode = godmode;
		localStorage.gameOver = gameOver;
		localStorage.gameOverTime = gameOverTime;
		localStorage.repRank = repRank;
		localStorage.logSize = logSize;
		localStorage.mapLevel = mapLevel;
		localStorage.styleLevel = styleLevel;
		localStorage.radius = radius;
		localStorage.casinoNet = casinoNet;
		localStorage.gotAxe = gotAxe;
		localStorage.gotClock = gotClock;
		localStorage.gotCompass = gotCompass;
		localStorage.gotKeyboard = gotKeyboard;
		localStorage.gotSeedling = gotSeedling;
		localStorage.noMoves = noMoves;
		localStorage.noEnergyUsed = noEnergyUsed;
		localStorage.noCoinGot = noCoinGot;
		localStorage.noCoinSpent = noCoinSpent;
		localStorage.noWoodChopped = noBuildingsErected;
		localStorage.noBuildingsErected = noBuildingsErected;
		localStorage.noQuestsCompleted = noQuestsCompleted;
		localStorage.noJunkFound = noJunkFound;
		localStorage.noFlowersFound = noFlowersFound;
		localStorage.noEventsWonCatch = noEventsWonCatch;
		localStorage.noEventsWonMath = noEventsWonMath;
		localStorage.noEventsWonFight = noEventsWonFight;
		localStorage.noEventsWonPattern = noEventsWonPattern;
		localStorage.noEventsLostCatch = noEventsLostCatch;
		localStorage.noEventsLostMath = noEventsLostMath;
		localStorage.noEventsLostFight = noEventsLostFight;
		localStorage.noEventsLostPattern = noEventsLostPattern;
		localStorage.onQuest = onQuest;
		localStorage.questDone = questDone;
		localStorage.questTown = questTown;
		localStorage.questType = questType;
		localStorage.junkPosX = junkPosX;
		localStorage.junkPosY = junkPosY;
		localStorage.junkingPosX = junkingPosX;
		localStorage.junkingPosY = junkingPosY;
		localStorage.junkCurrent = junkCurrent;
		localStorage.flowerPosX = flowerPosX;
		localStorage.flowerPosY= flowerPosY;
		localStorage.floristPosX= floristPosX;
		localStorage.floristPosY= floristPosY;
		localStorage.flowerCurrent= flowerCurrent;
		localStorage.flowerActive= flowerActive;
		localStorage.questSavedCell = questSavedCell;
		localStorage.completeMap = JSON.stringify(map);
		localStorage.completeTowns = JSON.stringify(towns);
		addToLog(gameSaved);
	} else {
		alert(ErrorLocalStorage);
	}
}

function load () {
	var k = 0;

	seed = parseInt(localStorage.seed);
	position[0] = parseInt(localStorage.posX);
	position[1] = parseInt(localStorage.posY);
	rep = parseInt(localStorage.rep);
	time = parseInt(localStorage.time);
	energy = parseInt(localStorage.energy);
	coin = parseInt(localStorage.coin);
	wood = parseInt(localStorage.wood);
	food = parseInt(localStorage.food);
	quick = parseInt(localStorage.quick);
	smart = parseInt(localStorage.smart);
	tough = parseInt(localStorage.tough);
	quickProgress = parseInt(localStorage.quickProgress);
	smartProgress = parseInt(localStorage.smartProgress);
	toughProgress = parseInt(localStorage.toughProgress);
	energyRevealed = parseInt(localStorage.energyRevealed);
	godmode = parseInt(localStorage.godmode);
	gameOver = parseInt(localStorage.gameOver);
	gameOverTime = parseInt(localStorage.gameOverTime);
	repRank = parseInt(localStorage.repRank);
	logSize = parseInt(localStorage.logSize);
	mapLevel = parseInt(localStorage.mapLevel);
	styleLevel = parseInt(localStorage.styleLevel);
	radius = parseInt(localStorage.radius);
	casinoNet = parseInt(localStorage.casinoNet);
	gotAxe = parseInt(localStorage.gotAxe);
	gotClock = parseInt(localStorage.gotClock);
	gotCompass = parseInt(localStorage.gotCompass);
	gotKeyboard = parseInt(localStorage.gotKeyboard);
	gotSeedling = parseInt(localStorage.gotSeedling);
	noMoves = parseInt(localStorage.noMoves);
	noEnergyUsed = parseInt(localStorage.noEnergyUsed);
	noCoinGot = parseInt(localStorage.noCoinGot);
	noCoinSpent = parseInt(localStorage.noCoinSpent);
	noWoodChopped = parseInt(localStorage.noWoodChopped);
	noBuildingsErected = parseInt(localStorage.noBuildingsErected);
	noQuestsCompleted = parseInt(localStorage.noQuestsCompleted);
	noJunkFound = parseInt(localStorage.noJunkFound);
	noFlowersFound = parseInt(localStorage.noFlowersFound);
	noEventsWonCatch = parseInt(localStorage.noEventsWonCatch);
	noEventsWonMath = parseInt(localStorage.noEventsWonMath);
	noEventsWonFight = parseInt(localStorage.noEventsWonFight);
	noEventsWonPattern = parseInt(localStorage.noEventsWonPattern);
	noEventsLostCatch = parseInt(localStorage.noEventsLostCatch);
	noEventsLostMath = parseInt(localStorage.noEventsLostMath);
	noEventsLostFight = parseInt(localStorage.noEventsLostFight);
	noEventsLostPattern = parseInt(localStorage.noEventsLostPattern);
	onQuest = parseInt(localStorage.onQuest);
	questDone = parseInt(localStorage.questDone);
	questTown = parseInt(localStorage.questTown);
	questType = parseInt(localStorage.questType);
	junkPosX = parseInt(localStorage.junkPosX);
	junkPosY = parseInt(localStorage.junkPosY);
	junkingPosX = parseInt(localStorage.junkingPosX);
	junkingPosY = parseInt(localStorage.junkingPosY);
	junkCurrent = parseInt(localStorage.junkCurrent);
	flowerPosX = parseInt(localStorage.flowerPosX);
	flowerPosY = parseInt(localStorage.flowerPosY);
	floristPosX = parseInt(localStorage.floristPosX);
	floristPosY = parseInt(localStorage.floristPosY);
	flowerCurrent = parseInt(localStorage.flowerCurrent);
	flowerActive = parseInt(localStorage.flowerActive);
	questSavedCell = localStorage.questSavedCell;
	map = JSON.parse(localStorage.completeMap);
	towns = JSON.parse(localStorage.completeTowns);

	seedInit = xmur3(seed.toString());
	rand = mulberry32(seedInit());

	if (godmode) enableGodmode();
	if (coin) coinEl.style.visibility = "";
	if (wood) woodEl.style.visibility = "";
	if (food) foodEl.style.visibility = "";
	if ((quick > 1) || (smart > 1) || (tough > 1)) {
		quickEl.style.visibility = "";
		smartEl.style.visibility = "";
		toughEl.style.visibility = "";
	}

	update();
	cellUpgradeLevel = map[position[0]][position[1]][2];
	cellUpgradeLevelCost = (cellUpgradeLevel + 1) * 12;
	updateIncome();
	updateWoodPrices();
	updateCurrentTown();
	updateRep();
	updateEnergy();
	updateStats();
	updateStatsAll();
	updateMap();
	updateStyle();
	addToLog(gameLoaded);
}
