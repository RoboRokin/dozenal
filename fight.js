function clickFight () {
	var enemy;
	var noClicks = 10;
	var enemySize = 50;

	if (level < 3) fightNoEnemies = 1;
	else if (level < 6) fightNoEnemies = randomInt (1,2);
	else if (level < 9) fightNoEnemies = randomInt (1,3);
	else fightNoEnemies = randomInt (1,4);

	noClicks = (level + 1) * 12;
	if (isQuest == 1) noClicks += Math.floor(noClicks / 12);
	for (var i = 0; i < 5; i++) fightHp[i] = 0;

	for (var i = 0; i < fightNoEnemies; i++) {
		fightHp[i] = fightHpMax[i] = Math.floor(noClicks / fightNoEnemies);
		fightHp[i] = fightHp[i] / 2;
		enemy = document.createElement("button");
		screen.appendChild(enemy);
		enemy.className = "enemy";
		enemySize = randomInt(50,100);
		enemy.style.height = enemySize + "px";
		enemy.style.width = enemySize + "px";
		enemy.style.top = randomInt(0, (eventScreenSizeY - enemySize)) + "px";
		enemy.style.left = randomInt(0, (eventScreenSizeX - enemySize)) + "px";
		enemy.setAttribute("id", "enemyNo" + i);
		enemy.onclick = function() { enemyClicked(this.id); };
	}

	refreshEnemies();
	fightInterval = setInterval(incHp,1000);
}

function enemyClicked (name) {
	switch (name) {
		case "enemyNo0": fightHp[0] -= tough; break;
		case "enemyNo1": fightHp[1] -= tough; break;
		case "enemyNo2": fightHp[2] -= tough; break; 
		case "enemyNo3": fightHp[3] -= tough; break;
		case "enemyNo4": fightHp[4] -= tough; break;
	}

	refreshEnemies();

	if ((fightHp[0] <= 0) && (fightHp[1] <= 0) && (fightHp[2] <= 0) && (fightHp[3] <=0 ) && (fightHp[4] <= 0)) {
		fightEnd(1);
	}
}

function incHp () {
	for (var i = 0; i < fightNoEnemies; i++) {
		if (fightHp[i] > 0) {
			fightHp[i] += Math.floor(fightHpMax[i] / 12);
			if (fightHp[i] >= fightHpMax[i]) fightEnd(0);
		}
	}
	refreshEnemies();
}

function refreshEnemies () {
	var percent;
	var currentEnemy;

	for (var i = 0; i < fightNoEnemies; i++) {
		percent = fightHp[i] / fightHpMax[i] * 100;
		currentEnemy = document.getElementById("enemyNo" + i);
		currentEnemy.innerHTML = dozenal(Math.ceil(fightHp[i]));

		if (percent > 0) {
			currentEnemy.style.backgroundImage = "linear-gradient(to top, red " + percent + "%, white " + percent + "%)";
		} else {
			currentEnemy.style.backgroundImage = "linear-gradient(to top, gray 100%, gray 100%)";
			currentEnemy.onclick = "";
		}
	}
}

function fightEnd (win) {
	for (var i = 0; i < fightNoEnemies; i++) {
		document.getElementById("enemyNo" + i).onclick = "";
	}

	if (win) eventMsgEl.innerHTML = strFightWon;
	else eventMsgEl.innerHTML = strFightLost;

	clearInterval(fightInterval);
	endEvent(win);
}
