function dozenal (decimal) {
	var negative = 0;

	if (decimal < 0) {
		negative = 1;
		decimal = Math.abs(decimal);
	}

	var quot = Math.floor(decimal / 12);
	var rema = decimal % 12;

	rema = String(rema);
	switch (rema) {
		case "10": rema = "ᘔ"; break;
		case "11": rema = "Ɛ"; break;
	}

	if (quot.toFixed(0) == "0") {
		if (negative == 0) { return rema; }
		else { return "-" + rema; }
	} else {
		quot = dozenal(Math.floor(quot));
		quot = String(quot);
		if (negative == 0) { return (quot + rema); }
		else { return "-" + (quot + rema); }
	}
}

function dozenalPercent (decimal) {
	var doz;

	doz = 144 / (100 / decimal);
	doz = dozenal(Math.floor(doz));

	return doz + "%";
}

function randomInt (min, max) {
	return Math.floor(rand() * (max - min + 1)) + min;
}

function updateRep () {
	if (godmode) { repRank = 0; }

	else if ((Math.log(rep) / Math.log(12)) >= repRank) {
		repRank++;
		switch (repRank) {
			case 1: break;
			case 2: addToLog(gameLoaded); break;
			case 3: addToLog(repRankPromotions[3]); break;
			case 4: addToLog(repRankPromotions[4]); break;
			case 5: addToLog(repRankPromotions[5]); break;
			case 6: addToLog(repRankPromotions[6]); break;
			case 7: addToLog(repRankPromotions[7]); break;
			default: addToLog(repRankPromotions[0]); break;
		}
	}

	repTitle = repTitles[repRank];
	if (repRank > repRankMax) repTitle += strLvl + dozenal(repRank - repRankMax);

	repEl.innerHTML = strRep + ": " + dozenal(rep) + " (" + repTitle + ")";
}

function incRep () {
	rep++;
	updateRep();
}

function incTime () {
	time++;
	updateTime();

	if (time % 12 == 0) forestGrow();
	if ((time % 12 == 0) && (logSize < logSizeMax)) upgradeMemory();

	energyChange(1);
	updateEnergy();

	if (coinAuto) {
		coinChange(coinAuto);
		updateCoin();
	}
}

function update () {
	cell = map[position[0]][position[1]][0];
	updateStats();
	updateButtons();
}

function updateStats() {
	updateTime();
	updateEnergy();
	updateCoin();
	updateLocation();

	if (wood) { woodEl.style.visibility = ""; }
	if (food) { foodEl.style.visibility = ""; }

	woodEl.innerHTML = strWood + ": " + dozenal(wood);
	foodEl.innerHTML = strFood + ": " + dozenal(food);
	quickEl.innerHTML = strQuick + ": " + dozenal(quick);
	smartEl.innerHTML = strSmart + ": " + dozenal(smart);
	toughEl.innerHTML = strTough + ": " + dozenal(tough);

	if (gotCompass) {
		statPositionEl.innerHTML = strPosition + ": " + dozenal(position[0]) + "," + dozenal(position[1]);
		statLevelEl.innerHTML = strLevel + ": " + dozenal(level);
		statEventEl.innerHTML = strEvent + ": " + dozenalPercent(eventChance);
	}
}

function updateTime () {
	if (time) timeEl.style.visibility = "";

	if (gotClock) {
		timeEl.innerHTML = strTime + ": ";
		if (time >= 1440) { timeEl.innerHTML += dozenal(Math.floor(time / 1440)) + "d "; }
		if (time >= 60) { timeEl.innerHTML += dozenal(Math.floor((time % 1440) / 60)) + "h "; }
		timeEl.innerHTML += dozenal(time % 60) + "m";
	} else {
		timeEl.innerHTML = strTime + ": " + dozenal(time);
	}
}

function updateCoin () {
	coinEl.innerHTML = strCoin + ": " + dozenal(coin);
	if (coinAuto) { coinEl.innerHTML += " +" + dozenal(coinAuto); }
}

function updateIncome () {
	coinAuto = 0;

	for (var i = 0; i < towns.length; i++) {
		coinAuto += Math.floor(towns[i][3] / 12) * towns[i][4];
	}
	updateCoin();
}

function updateEnergy () {
	if (energyRevealed) energyEl.style.visibility = "";
	energyEl.innerHTML = strEnergy + ": " + dozenal(energy);
}

function updateLocation () {
	var location = "";
	var progress = "";
	var specific = "";

	locationEl.innerHTML = "";
	progressEl.innerHTML = "";
	specificEl.innerHTML = "";

	switch (cell) {
		case home:
			location = locationHomes[cellUpgradeLevel];
			if (noMoves) { specific += "<br>" + strLocUpgrade + ": " + dozenal(cellUpgradeLevelCost) + symbolWood; }
			break;
		case plain:
			location = locationPlains;
			specific += "<br>" + strLocErectCost + ": " + dozenal((noBuildingsErected + 1) * 12) + symbolWood;
			specific += "<br>" + strLocFarmCost + ": " + dozenal(level * 12) + symbolWood;
			break;
		case forest:
			location = locationForest;
			if (gotAxe) progress = dozenalPercent(map[position[0]][position[1]][2] * 100 / (12 * level));
			break;
		case hill:
			location = locationHill;
			break;
		case river:
			location = locationRiver;
			break;
		case bridge:
			location = locationBridge;
			break;
		case farm:
			location = locationFarm;
			break;
		case town:
			location = locationTown + ": " + towns[currentTown][2];
			if (towns[currentTown][3] > 0) {
				progress = strLocTownRep + ": " + dozenal(towns[currentTown][3]);
			}
			if (Math.floor(towns[currentTown][3] / 12) > 1) {
				specific += strLocIncome + ": " + dozenal(Math.floor(towns[currentTown][3] / 12));
			}
			if (towns[currentTown][4] > 0) {
				specific += "<br>" + strLocWorkers + ": " + dozenal(towns[currentTown][4]);
			}
			specific += "<br>" + strLocQuests + ": " + dozenal(towns[currentTown][5]);
			specific += "<br>" + strLocHireCost + ": " + dozenal(towns[currentTown][4] + 1) + symbolFood;
			if (questTown == currentTown) specific += "<br>" + strLocOnQuest;
			break;
		case market:
			location = locationMarket;
			specific += strLocWoodBuy + ": " + dozenal(priceWoodBuy) + symbolCoin;
			specific += "<br>" + strLocWoodSell + ": " + dozenal(priceWoodSell) + symbolCoin;
			if (!gotAxe) specific += "<br>" + strItemAxe + ": " + dozenal(priceAxe) + symbolCoin;
			if (!gotClock) specific += "<br>" + strItemClock + ": " + dozenal(priceClock) + symbolCoin;
			if (!gotCompass) specific += "<br>" + strItemCompass + ": " + dozenal(priceCompass) + symbolCoin;
			if (!gotKeyboard) specific += "<br>" + strItemKeyboard + ": " + dozenal(priceKeyboard) + symbolCoin;
			if (!gotSeedling) specific += "<br>" + strItemSeedling + ": " + dozenal(priceSeedling) + symbolCoin;
			if (mapLevel < mapLevelMax) specific += "<br>" + strLocMapUpgrade + ": " + dozenal(Math.pow(12, mapLevel)) + symbolCoin;
			if (styleLevel < styleLevelMax) specific += "<br>" + strLocStyleUpgrade + ": " + dozenal(Math.pow(12, styleLevel)) + symbolCoin;
			break;
		case casino:
			location = locationCasino;
			specific += "<br>" + strLocCurrentBet + ": " + dozenal(casinoBet0) + symbolCoin;
			specific += "<br>" + strLocNetWinnings + ": " + dozenal(casinoNet) + symbolCoin;
			break;
		case terminal:
			location = locationTerminal;
			specific += "<br>" + strLocTravelCost + ": " + dozenal(Math.floor(144 / (cellUpgradeLevel + 1))) + symbolCoin;
			break;
		case range:
			location = locationRange + strLvl + dozenal(cellUpgradeLevel + 1);
			progress = dozenalPercent(Math.floor(quickProgress * 100 / (quick * 12)));
			if (cellUpgradeLevel < 12) { specific += "<br>" + strLocUpgradeCost + ": " + dozenal(cellUpgradeLevelCost) + symbolWood; }
			if (quick < 143) { specific += "<br>" + strLocTrainerCost + ": " + dozenal(quick * quick) + symbolCoin; }
			break;
		case library:
			location = locationLibrary + strLvl + dozenal(cellUpgradeLevel + 1);
			progress = dozenalPercent(Math.floor(smartProgress * 100 / (smart * 12)));
			if (cellUpgradeLevel < 12) { specific += "<br>" + strLocUpgradeCost + ": " + dozenal(cellUpgradeLevelCost) + symbolWood; }
			if (smart < 143) { specific += "<br>" + strLocTrainerCost + ": " + dozenal(smart * smart) + symbolCoin; }
			break;
		case gym:
			location = locationGym + strLvl + dozenal(cellUpgradeLevel + 1);
			progress = dozenalPercent(Math.floor(toughProgress * 100 / (tough * 12)));
			if (cellUpgradeLevel < 12) { specific += "<br>" + strLocUpgradeCost + ": " + dozenal(cellUpgradeLevelCost) + symbolWood; }
			if (tough < 143) { specific += "<br>" + strLocTrainerCost + ": " + dozenal(tough * tough) + symbolCoin; }
			break;
		case junking:
			location = locationJunking;
			specific += "<br>" + strLocJunkDelivered + ": " + dozenal(noJunkFound);
			break;
		case florist:
			location = locationFlorist;
			specific += "<br>" + strLocFlowersDelivered + ": " + dozenal(noFlowersFound);
			break;
		case bandits:
			location = locationBandits;
			break;
		case dungeon:
			location = locationDungeon;
			break;
		case puzzles:
			location = locationMystery;
			break;
		case quest:
			location = locationQuest;
			break;
	}

	locationEl.innerHTML = location;
	progressEl.innerHTML = progress;
	specificEl.innerHTML = specific;
}

function energyChange (input) {
	var allow = 1;
	var temp = energy + input;

	if (temp < 0) {
		allow = 0;
	} else if (temp > 12) {
		energy = 12;
	} else {
		energy += input;
	}

	if (energy == 0) { energyRevealed = 1; }

	updateEnergy();
	updateButtons();

	return allow;
}

function sleep () {
	energy = 12;
	addToLog(homeSlept);
}

function upgradeCell () {
	wood -= cellUpgradeLevelCost;
	map[position[0]][position[1]][2]++;

	switch (cell) {
		case home: addToLog(homeIntros[cellUpgradeLevel]); break;
		case gym: addToLog(upgradedGym); break;
		case range: addToLog(upgradedRange); break;
		case library: addToLog(upgradedLibrary); break;
		case terminal: addToLog(upgradedTerminal); break;
	}

	cellUpgradeLevel = map[position[0]][position[1]][2];
	cellUpgradeLevelCost = (cellUpgradeLevel + 1) * 12;
	update();
}

function updateMap () {
	if (mapLevel >= 7) mapImages = 1;
	if (mapLevel >= 4) mapColors = 1;
	
	switch (mapLevel) {
		case 2: viewSize = 2; break;
		case 3:
		case 4: viewSize = 3; break;
		case 5: viewSize = 4; break;
		case 6:
		case 7:
		case 8: viewSize = 5; break;
		default: viewSize = 1; break;
	}

	displayMap();
	updateStyle();
}

function upgradeMap () {
	coinChange(-(Math.pow(12, mapLevel)));
	mapLevel++;
	updateMap();
	updateButtons();
	addToLog(upgradedMap);
}

function upgradeMemory () {
	logSize++;
	addToLog(upgradedMemory);
}

function upgradeStyle () {
	coinChange(-(Math.pow(12, styleLevel)));
	styleLevel++;
	updateStyle();
	updateButtons();
	addToLog(upgradedStyle);
}

function rest () {
	var randomIndex = randomInt(0, restNotes.length-1);
	energyChange(1);
	addToLog(restNotes[randomIndex]);

	if (randomInt(1, 144) == 1) { startEvent(randomInt(1, 4), 0); }
}

function checkForBorder (i, j) {
	if (((i == mapCenter - radius -1) && ((j >= mapCenter - radius -1) && (j <= mapCenter + radius +1))) ||
		((i == mapCenter + radius +1) && ((j >= mapCenter - radius -1) && (j <= mapCenter + radius +1))) ||
		((j == mapCenter - radius -1) && ((i >= mapCenter - radius -1) && (i <= mapCenter + radius +1))) ||
		((j == mapCenter + radius +1) && ((i >= mapCenter - radius -1) && (i <= mapCenter + radius +1)))) {
		return true;
	}

		return false;
}

function getCellColor (x, y) {
	switch (map[x][y][0]) {
		case (plain): return "yellow"; break;
		case (forest): return "green"; break;
		case (hill): return "orange"; break;
		case (river): return "blue"; break;
		case (quest): return "red"; break;
		default: return "white"; break;
	}
}

function displayMap () {
	var cellClass = "white";
	var minimapContent = "";

	for (var i = position[0] - viewSize; i <= position[0] + viewSize; i++) {
		for (var j = position[1] - viewSize; j <= position[1] + viewSize; j++) {
			if ((i >= 0) && (j >= 0) && (i < mapSize) && (j < mapSize)) {
				if ((i == position[0]) && (j == position[1])) {
					if (mapImages) minimapContent += "<div class='minimapCell'>" + svgPlayer + "</div>";
					else if (mapColors) minimapContent += "<div class='minimapCell red'>" + player + "</div>";
					else minimapContent += "<div class='minimapCell'>" + player + "</div>";
				} else if (map[i][j][1] != 0) {
					if (mapColors) cellClass = getCellColor(i, j);
					if (mapImages) {
						minimapContent += "<div class='minimapCell'>";
						switch (map[i][j][0]) {
							case home: minimapContent += svgHome; break;
							case plain: minimapContent += svgPlain; break;
							case forest: minimapContent += svgForest; break;
							case hill: minimapContent += svgHill; break;
							case river: minimapContent += svgRiver; break;
							case border: minimapContent += svgBorder; break;
							case bridge: minimapContent += svgBridge; break;
							case farm: minimapContent += svgFarm; break;
							case town: minimapContent += svgTown; break;
							case market: minimapContent += svgMarket; break;
							case casino: minimapContent += svgCasino; break;
							case terminal: minimapContent += svgTerminal; break;
							case gym: minimapContent += svgGym; break;
							case range: minimapContent += svgRange; break;
							case library: minimapContent += svgLibrary; break;
							case quest: minimapContent += svgQuest; break;
							case junking: minimapContent += svgJunking; break;
							case florist: minimapContent += svgFlorist; break;
							default: minimapContent += map[i][j][0]; break;
						}
						minimapContent += "</div>"
					} else minimapContent += "<div class=" + cellClass + ">" + map[i][j][0] + "</div>";
				} else {
					minimapContent += "<div>&nbsp</div>";
				}
			} else {
					minimapContent += "<div>&nbsp</div>";
			}
		}
		minimapContent += '<br>';
	}

	mapminiEl.innerHTML = minimapContent;
}

function displayMapFull () {
	var cellColor;
	var currentCellName;

	for (var i = 0; i < mapSize; i++) {
		for (var j = 0; j < mapSize; j++) {
			currentCellName = document.getElementById(i + "+" + j);
			if ((i == position[0]) && (j == position[1])) {
				if (mapColors) currentCellName.className = "red";
				if (mapImages) currentCellName.innerHTML = svgPlayer;
				else currentCellName.innerHTML = player;
			} else if (map[i][j][1] != 0) {
				if (mapColors) currentCellName.className = getCellColor(i, j);
				if (mapImages) {
					switch (map[i][j][0]) {
						case home: currentCellName.innerHTML = svgHome; break;
						case plain: currentCellName.innerHTML = svgPlain; break;
						case forest: currentCellName.innerHTML = svgForest; break;
						case hill: currentCellName.innerHTML = svgHill; break;
						case river: currentCellName.innerHTML = svgRiver; break;
						case border: currentCellName.innerHTML = svgBorder; break;
						case bridge: currentCellName.innerHTML = svgBridge; break;
						case farm: currentCellName.innerHTML = svgFarm; break;
						case town: currentCellName.innerHTML = svgTown; break;
						case market: currentCellName.innerHTML = svgMarket; break;
						case casino: currentCellName.innerHTML = svgCasino; break;
						case terminal: currentCellName.innerHTML = svgTerminal; break;
						case gym: currentCellName.innerHTML = svgGym; break;
						case range: currentCellName.innerHTML = svgRange; break;
						case library: currentCellName.innerHTML = svgLibrary; break;
						case quest: currentCellName.innerHTML = svgQuest; break;
						case junking: currentCellName.innerHTML = svgJunking; break;
						case florist: currentCellName.innerHTML = svgFlorist; break;
						default: currentCellName.innerHTML = map[i][j][0]; break;
					}
				} else currentCellName.innerHTML = map[i][j][0];
			} else {
				currentCellName.innerHTML = "&nbsp";
			}
		}
	}
}

function addToLog (message) { 
	var node = document.createElement("div");
	var textNode = document.createTextNode(message);

	node.className = "logEntry";
	node.appendChild(textNode);
	if (logEl.getElementsByTagName("div").length > logSize) logEl.removeChild(logEl.lastChild);
	logEl.insertBefore(node, logEl.firstChild);
}

function coinChange (input) {
	coinEl.style.visibility = "";

	if (coin + input < 0) {
		return false;
	} else {
		coin += input;
		updateCoin();
	}

	if ((!gameOver) && (coin > coinGoal)) {
		gameOver = 1;
		gameOverTime = time;
		addToLog(strGameOver + time);
	}

	return true;
}

function moveAllow (destination) {
	if (energy < 1) return false;
	if (destination == border) return false;
	if ((destination == river) && (energy < energyCostRiver)) return false;
	if ((destination == hill) && (energy < energyCostHill)) return false;
	if ((destination == forest) && (energy < energyCostForest)) return false;
	return true;
}

function moveWithKeys (key) {
	var direction;
	var destination;

	switch (key) {
		case 37:
			direction = 4;
			destination = map[position[0]][position[1] - 1][0];
			break;
		case 38:
			direction = 2;
			destination = map[position[0] - 1][position[1]][0];
			break;
		case 39:
			direction = 6;
			destination = map[position[0]][position[1] + 1][0];
			break;
		case 40:
			direction = 8;
			destination = map[position[0] + 1][position[1]][0];
			break;
	}

	if (moveAllow(destination)) {
		incRep();
		moveDirection(direction);
	}
}

function moveDirection (direction) {
	switch (direction) {
		case 2:
			addToLog(logMoveUp);
			move(position[0] - 1, position[1]);
			break;
		case 4:
			addToLog(logMoveLeft);
			move(position[0], position[1] - 1);
			break;
		case 6:
			addToLog(logMoveRight);
			move(position[0], position[1] + 1);
			break;
		case 8:
			addToLog(logMoveDown);
			move(position[0] + 1, position[1]);
			break;
	}
}

function move (x, y) {
	position[0] = x;
	position[1] = y;
	noMoves++;
	map[position[0]][position[1]][1] = 1;
	cell = map[position[0]][position[1]][0];
	cellUpgradeLevel = map[position[0]][position[1]][2];
	cellUpgradeLevelCost = (cellUpgradeLevel + 1) * 12;
	level = Math.max(Math.abs(position[0] - mapCenter), Math.abs(position[1] - mapCenter));

	if (cell == town) updateCurrentTown();
	if ((junkCurrent == 0) && (position[0] == junkPosX) && (position[1] == junkPosY)) junkFound();
	if ((position[0] == flowerPosX) && (position[1] == flowerPosY)) flowerFound();

	bgTheme = 0;
	switch (cell) {
		case plain:
			energyChange(-energyCostPlain);
			bgTheme = 1;
			audioPlain.muted = false;
			audioForest.muted = true;
			break;
		case forest:
			energyChange(-energyCostForest);
			bgTheme = 2;
			audioForest.muted = false;
			audioPlain.muted = true;
			break;
		case hill:
			energyChange(-energyCostHill);
			bgTheme = 3;
			break;
		case river:
			energyChange(-energyCostRiver);
			bgTheme = 4;
			break;
		case junking: energyChange(-1); junkingVisited(); break;
		case florist: energyChange(-1); floristVisited(); break;
		case bandits: energyChange(-1); startEvent(1,0); break;
		case dungeon: energyChange(-1); startEvent(2,0); break;
		case puzzles: energyChange(-1); startEvent(3,0); break;
		default: energyChange(-1); break;
	}

	update();
	displayMap();
	updateStats();
	updateStyle();

	eventChance = 0;
	if ((quick < 144) && ((cell == plain) || (cell == forest) || (cell == hill))) {
		eventChance = Math.floor(((level * 12) - quick) * 100 / 144);
		if (eventChance < 1) eventChance = 0;
	}

	if (cell == quest) {
		if (questDone) endQuest();
		else startEvent(questType, 1);
	} else if ((quick < 144) && (cell == plain) || (cell == forest) || (cell == hill)) {
		if (randomInt(0, 100) < eventChance) startEvent(randomInt(1, 4), 0);
	}
}

function incAttribute (attribute) {
	switch (attribute) {
		case 1:
			if (quick < 143) {
				quick++;
				addToLog(upgradedQuick);
			} break;
		case 2:
			if (smart < 143) {
				smart++;
				addToLog(upgradedSmart);
			} break;
		case 3:
			if (tough < 143) {
				tough++;
				addToLog(upgradedTough);
			} break;
	}

	quickEl.style.visibility = "";
	smartEl.style.visibility = "";
	toughEl.style.visibility = "";
	updateStats();
}

function practice () {
	for (var i = 0; i <= map[position[0]][position[1]][2]; i++) {
		switch (cell) {
			case range:
				quickProgress++;
				if (quickProgress >= quick * 12) {
					quickProgress = 0;
					incAttribute(1);
				} break;
			case library:
				smartProgress++;
				if (smartProgress >= smart * 12) {
					smartProgress = 0;
					incAttribute(2);
				} break;
			case gym:
				toughProgress++;
				if (toughProgress >= tough * 12) {
					toughProgress = 0;
					incAttribute(3);
				} break;
		}
	}
	update();
}

function train () {
	switch (cell) {
		case range:
			coin -= quick * quick;
			incAttribute(1);
			break;
		case library:
			coin -= smart * smart;
			incAttribute(2);
			break;
		case gym:
			coin -= tough * tough;
			incAttribute(3);
			break;
	}
	energyChange(-1);
	update();
}

function updateCurrentTown () {
	if ((position[0] == towns[0][0]) && (position[1] == towns[0][1])) { currentTown = 0; }
	else if ((position[0] == towns[1][0]) && (position[1] == towns[1][1])) { currentTown = 1; }
	else if ((position[0] == towns[2][0]) && (position[1] == towns[2][1])) { currentTown = 2; }
}

function checkForTown () {
	if ((map[position[0] + 1][position[1]][0] == town) ||
		(map[position[0] - 1][position[1]][0] == town) ||
		(map[position[0]][position[1] + 1][0] == town) ||
		(map[position[0]][position[1] - 1][0] == town) ||
		(map[position[0] + 1][position[1] + 1][0] == town) ||
		(map[position[0] + 1][position[1] - 1][0] == town) ||
		(map[position[0] - 1][position[1] + 1][0] == town) ||
		(map[position[0] - 1][position[1] - 1][0] == town)) {
			return true;
		}

	return false;
}

function buildFarm () {
	wood -= level * 12;
	map[position[0]][position[1]][0] = farm;
	food++;
	foodEl.style.visibility = "";
	update();
}

// keyboard controls
document.onkeydown = function(evt) {
	evt = evt || window.event;
	switch (evt.keyCode) {
		case 37:
		case 38:
		case 39:
		case 40: if (gotKeyboard && keysEnabled) moveWithKeys(evt.keyCode); break;
		case 66: enableGodmode(); break; // b
	}
}

function enableGodmode() {
	godmode = 1;
	mapLevel = 8;
	coinChange(100000);
	rep += 1000000;
	updateRep();
	wood = 999;
	food = 999;
	gotAxe = 1;
	gotClock = 1;
	gotCompass = 1;
	gotKeyboard = 1;
	keysEnabled = 1;
	gotSeedling = 1;
	quick = smart = tough = 144;
	energyEl.style.visibility = "";
	woodEl.style.visibility = "";
	foodEl.style.visibility = "";
	quickEl.style.visibility = "";
	smartEl.style.visibility = "";
	toughEl.style.visibility = "";
	for (var i = 0; i < mapSize; i++) {
		for (var j = 0; j < mapSize; j++) {
			map[i][j][1] = 1;
		}
	}
	updateMap();
}

function erect (cell, building) {
	wood -= (noBuildingsErected + 1) * 12;
	noBuildingsErected++;
	erecting = 0;
	map[position[0]][position[1]][0] = cell;
	map[position[0]][position[1]][2] = 0;
	addToLog(strErected + building);
	update();
}

function buy (item) {
	switch (item) {
		case 1:
			gotAxe = 1;
			coinChange(-priceAxe);
			addToLog(strBuyAxe);
			break;
		case 2:
			gotClock = 1;
			coinChange(-priceClock);
			addToLog(strBuyClock);
			updateTime();
			break;
		case 3:
			gotCompass = 1;
			coinChange(-priceCompass);
			addToLog(strBuyCompass);
			statPositionEl.style.visibility = "visible";
			updateStats();
			break;
		case 4:
			gotKeyboard = 1;
			keysEnabled = 1;
			coinChange(-priceKeyboard);
			addToLog(strBuyKeyboard);
			break;
		case 6:
			gotSeedling = 1;
			coinChange(-priceSeedling);
			addToLog(strBuySeedling);
			break;
		default: break;
	}

	shopping = 0;
	updateLocation();
	updateButtons();
}

function forestPlace (x, y) {
	map[x][y][0] = forest;
	map[x][y][2] = 0;
	if (map[x + 1][y][0] == plain) map[x + 1][y][2] = 1;
	if (map[x - 1][y][0] == plain) map[x - 1][y][2] = 1;
	if (map[x][y + 1][0] == plain) map[x][y + 1][2] = 1;
	if (map[x][y - 1][0] == plain) map[x][y - 1][2] = 1;
	updateMap();
}

function forestGrow () {
	for (var i = 0; i < mapSize; i++) {
		for (var j = 0; j < mapSize; j++) {
			if ((map[i][j][0] == plain) && (map[i][j][2])) map[i][j][2]++;
			if (map[i][j][2] >= 36) {
				forestPlace(i, j);
			}
		}
	}
	displayMapFull();
}

function klik (button) {
	incRep();

	if (erecting) {
		switch (button) {
			case 1: erect(home, locationHome); break;
			case 2: erect(market, locationMarket); break;
			case 3: erect(casino, locationCasino); break;
			case 4: erect(gym, locationGym); break;
			case 5: erecting = 0; update(); break;
			case 6: erect(range, locationRange); break;
			case 7: erect(terminal, locationTerminal); break;
			case 8: erect(library, locationLibrary); break;
			case 9: erect(farm, locationFarm); break;
		}
		return;
	}

	if (shopping) {
		switch (button) {
			case 1: buy(1); break;
			case 2: buy(2); break;
			case 3: buy(3); break;
			case 4: buy(4); break;
			case 5: shopping = 0; update(); break;
			case 6: buy(6); break;
		}
		return;
	}

	if (cell == start) {
		warmup();
		updateStats();
		return;
	}

	if ((button == 2) || (button == 4) || (button == 6) || (button == 8)) {
		moveDirection(button);
		return;
	}

	switch (cell) {
		case home:
			switch (button) {
				case 1: upgradeCell(); break;
				case 3: upgradeMemory(); break;
				case 5: sleep(); break;
				case 7: load(); break;
				case 9: save(); break;
			} break;
		case plain:
			switch (button) {
				case 1: erecting = 1; updateButtons(); break;
				case 5: rest(); break;
				case 7: forestPlace(position[0], position[1]); gotSeedling = 0; update(); break;
				case 9: startEvent(1, 0); break;
			} break;
		case forest:
			switch (button) {
				case 1: chopWood(); break;
				case 5: rest(); break;
			} break;
		case hill:
		case river:
		case bridge:
			switch (button) {
				case 5: rest(); break;
			} break;
		case farm:
			switch (button) {
				case 1: rest(); break;
				case 5: farmFood(); break;
			} break;
		case town:
			switch (button) {
				case 1: rest(); break;
				case 5: work(); break;
				case 7: hire(); break;
				case 9: questToggle(); break;
			} break;
		case market:
			switch (button) {
				case 1: upgradeStyle(); break;
				case 3: upgradeMap(); break;
				case 5: shopping = 1; updateButtons(); break;
				case 7: trade(0); break;
				case 9: trade(1); break;
			} break;
		case casino:
			switch (button) {
				case 5: gamble(); break;
				case 1:
				case 3:
				case 7:
				case 9: changeBet(button); break;
			} break;
		case terminal:
			switch (button) {
				case 1:
				case 3:
				case 7:
				case 9: travel(button); break;
			} break;
		case gym:
		case range:
		case library:
			switch (button) {
				case 1: upgradeCell(); break;
				case 3: train(); break;
				case 5: if (energyChange(-1)) { practice(); } break;
			} break;
	}
	updateStats();
}

function displayButton (button, isDisabled, text) {
	button.disabled = isDisabled;
	button.innerHTML = text;
	if (isDisabled) { button.style.visibility = "hidden"; }
	else { button.style.visibility = "visible"; }
}

function updateButtonMove (button) {
	var action;
	var destination;

	switch (button) {
		case button2:
			action = actionMoveUp;
			destination = map[position[0] - 1][position[1]][0];
			break;
		case button4:
			action = actionMoveLeft;
			destination = map[position[0]][position[1] - 1][0];
			break;
		case button6:
			action = actionMoveRight;
			destination = map[position[0]][position[1] + 1][0];
			break;
		case button8:
			action = actionMoveDown;
			destination = map[position[0] + 1][position[1]][0];
			break;
	}

	if (moveAllow(destination)) displayButton(button, false, action);
}

function clearButtons () {
	displayButton(button1, true, "1");
	displayButton(button2, true, "2");
	displayButton(button3, true, "3");
	displayButton(button4, true, "4");
	displayButton(button5, true, "5");
	displayButton(button6, true, "6");
	displayButton(button7, true, "7");
	displayButton(button8, true, "8");
	displayButton(button9, true, "9");
}

function updateButtons () {
	clearButtons();

	if (erecting) {
		displayButton(button5, false, strNothing);
		if (wood >= (noBuildingsErected + 1) * 12) {
			displayButton(button1, false, locationHome);
			displayButton(button2, false, locationMarket);
			displayButton(button3, false, locationCasino);
			displayButton(button4, false, locationGym);
			displayButton(button6, false, locationRange);
			displayButton(button7, false, locationTerminal);
			displayButton(button8, false, locationLibrary);
		}
		if (wood >= level * 12) displayButton(button9, false, locationFarm);
		return;
	}

	if (shopping) {
		displayButton(button5, false, strNothing);
		if ((!gotAxe) && (coin >= priceAxe)) displayButton(button1, false, strItemAxe);
		if ((!gotClock) && (coin >= priceClock)) displayButton(button2, false, strItemClock);
		if ((!gotCompass) && (coin >= priceCompass)) displayButton(button3, false, strItemCompass);
		if ((!gotKeyboard) && (coin >= priceKeyboard)) displayButton(button4, false, strItemKeyboard);
		if ((!gotSeedling) && (coin >= priceSeedling)) displayButton(button6, false, strItemSeedling);
		return;
	}

	updateButtonMove(button2);
	updateButtonMove(button4);
	updateButtonMove(button6);
	updateButtonMove(button8);

	switch (cell) {
		case home:
			displayButton(button5, false, actionSleep);
			if (energy > 0) {
				if ((cellUpgradeLevel < 11) && (wood >= cellUpgradeLevelCost)) { displayButton(button1, false, actionUpgrade); }
				displayButton(button7, false, actionLoad);
				displayButton(button9, false, actionSave);
			}
			break;
		case plain:
			// displayButton(button9, false, actionEvent); // TESTING
			if (energy > 0) {
				if (wood >= (noBuildingsErected + 1) * 12) displayButton(button1, false, actionErect);
			}
			displayButton(button5, false, actionRest);
			if (gotSeedling) displayButton(button7, false, actionPlant);
			break;
		case forest:
			displayButton(button5, false, actionRest);
			if ((gotAxe) && (energy > 0)) { displayButton(button1, false, actionChop); }
			break;
		case hill:
			displayButton(button5, false, actionRest);
			break;
		case river:
			displayButton(button5, false, actionRest);
			break;
		case bridge:
			displayButton(button5, false, actionRest);
			break;
		case farm:
			displayButton(button1, false, actionRest);
			if (energy >= 12) displayButton(button5, false, actionFarm);
			break;
		case town:
			displayButton(button1, false, actionRest);
			if (energy > 0) displayButton(button5, false, actionWork);
			if ((food >= towns[currentTown][4] + 1) && (towns[currentTown][3] >= 12))
				displayButton(button7, false, actionHire);
			if (!onQuest) { displayButton(button9, false, actionQuest); }
			else if (questTown == currentTown) { displayButton(button9, false, actionQuit); }
			break;
		case market:
			displayButton(button5, false, actionShop);
			if ((mapLevel < mapLevelMax) && (coin >= Math.pow(12, mapLevel))) displayButton(button3, false, actionMap);
			if ((styleLevel < styleLevelMax) && (coin >= Math.pow(12, styleLevel))) displayButton(button1, false, actionStyle);
			if (wood) displayButton(button7, false, actionSell);
			if (coin >= priceWoodBuy) displayButton(button9, false, actionBuy);
			break;
		case casino:
			if (coin >= casinoBet0) { displayButton(button5, false, actionGamble); }
			displayButton(button1, false, dozenal(casinoBet1));
			displayButton(button3, false, dozenal(casinoBet2));
			displayButton(button7, false, dozenal(casinoBet3));
			displayButton(button9, false, dozenal(casinoBet4));
			break;
		case terminal:
			if (coin >= Math.floor((144 / (cellUpgradeLevel + 1)))) {
				displayButton(button1, false, locationHome);
				displayButton(button3, false, towns[0][2]);
				displayButton(button7, false, towns[1][2]);
				displayButton(button9, false, towns[2][2]);
			}
			break;
		case range:
			if (quick < 143) { displayButton(button5, false, actionSkate); }
			if ((cellUpgradeLevel < 11) && (wood >= cellUpgradeLevelCost)) {
				displayButton(button1, false, actionUpgrade);
			}
			if ((quick < 143) && (coin >= quick * quick) && (energy)) {
				displayButton(button3, false, actionTrain);
			}
			break;
		case library:
			if (smart < 143) { displayButton(button5, false, actionStudy); }
			if ((cellUpgradeLevel < 11) && (wood >= cellUpgradeLevelCost)) {
				displayButton(button1, false, actionUpgrade);
			}
			if ((smart < 143) && (coin >= smart * smart) && (energy)) {
				displayButton(button3, false, actionTrain);
			}
			break;
		case gym:
			if (tough < 143) { displayButton(button5, false, actionLift); }
			if ((cellUpgradeLevel < 11) && (wood >= cellUpgradeLevelCost)) {
				displayButton(button1, false, actionUpgrade);
			}
			if ((tough < 143) && (coin >= tough * tough) && (energy)) {
				displayButton(button3, false, actionTrain);
			}
			break;
		case junking: break;
		case bandits:
			button9.innerHTML = actionFight; break;
		case dungeon:
			button9.innerHTML = actionLoot; break;
		case puzzles:
			button9.innerHTML = actionScience; break;
		case quest:
			button9.innerHTML = actionQuest; break;
	}
}

function trade (trading) {
	switch (trading) {
		case 0:
			wood--;
			coin += priceWoodSell;
			addToLog(strMarketSellW + dozenal(priceWoodSell) + symbolCoin);
			break;
		case 1:
			wood++;
			coin -= priceWoodBuy;
			addToLog(strMarketBuyW + dozenal(priceWoodBuy) + symbolCoin);
			break;
	}
	update();
}

function updateWoodPrices () {
		var noForestCells = 0;

		for (var i = 0; i < mapSize; i++) {
			for (var j = 0; j < mapSize; j++) {
				if (map[i][j][0] == forest) { noForestCells++; }
			}
		}

		priceWood = Math.ceil((mapSize * mapSize) / (noForestCells + 1));
		priceWoodBuy = priceWood + Math.ceil(priceWood / 12);
		priceWoodSell = priceWood - Math.ceil(priceWood / 12);
}

function chopWood () {
	var gain;
	var goal = level * 12;

	if (energyChange(-1) == 1) {
		map[position[0]][position[1]][2] += tough;
		updateLocation();
	}

	if ((tough == 144) || (map[position[0]][position[1]][2] >= goal)) {
		gain = randomInt(12, 60) * level;
		wood += gain;

		map[position[0]][position[1]][0] = plain;
		map[position[0]][position[1]][2] = 0;

		addToLog(strChopped + dozenal(gain) + symbolWood);
		updateWoodPrices();
		update();
	}
}

function farmFood () {
	food++;
	energyChange(-12);
	updateButtons();
}

function work () {
	energyChange(-1);
	coinChange(smart);
	towns[currentTown][3]++; // rep
	updateIncome();
	addToLog(strWorked + dozenal(smart) + symbolCoin);
}

function hire () {
	towns[currentTown][3]++; // rep
	towns[currentTown][4]++; // workers
	food -= towns[currentTown][4];
	updateIncome();
	update();
}

function gamble () {
	if (randomInt(1, 12) > 6) {
		coinChange(casinoBet0);
		casinoNet += casinoBet0;
		addToLog(textGambleWin + dozenal(casinoBet0));
	} else {
		coinChange(-casinoBet0);
		casinoNet -= casinoBet0;
		addToLog(textGambleLose + dozenal(casinoBet0));
	}
	updateButtons();
	updateLocation();
}

function changeBet (button) {
	var temp = casinoBet0;
	switch (button) {
		case 1:
			casinoBet0 = casinoBet1;
			casinoBet1 = temp;
			break;
		case 3:
			casinoBet0 = casinoBet2;
			casinoBet2 = temp;
			break;
		case 7:
			casinoBet0 = casinoBet3;
			casinoBet3 = temp;
			break;
		case 9:
			casinoBet0 = casinoBet4;
			casinoBet4 = temp;
			break;
	}
	updateButtons();
	updateLocation();
}

function travel (button) {
	coin -= Math.floor((144 / (cellUpgradeLevel + 1)));

	switch (button) {
		case 1:
			move(mapCenter, mapCenter);
			break;
		case 3:
			move(towns[0][0], towns[0][1]);
			break;
		case 7:
			move(towns[1][0], towns[1][1]);
			break;
		case 9:
			move(towns[2][0], towns[2][1]);
			break;
	}
}

function updateStatsAll () {
	var stats = "";

	stats += "<div style='column-count:4'>" + strStats;
	if (gameOver)stats += "<br>" + strStatsGameOver + dozenal(gameOverTime);
	stats += "<br>" + strRep + ": " + dozenal(rep) + " - " + repTitle;
	stats += "<br>" + strTime + ": " + dozenal(time);
	stats += "<br>" + strEnergy + ": " + dozenal(energy);
	stats += "<br>" + strQuick + ": " + dozenal(quick);
	stats += "<br>" + strSmart + ": " + dozenal(smart);
	stats += "<br>" + strTough + ": " + dozenal(tough);
	stats += "<br>" + strStatsMoves + ": " + dozenal(noMoves);
	stats += "<br>" + strStatsEnergyUsed + ": " + dozenal(noEnergyUsed);
	stats += "<br>" + strStatsCoinEarned + ": " + dozenal(noCoinGot);
	stats += "<br>" + strStatsCoinSpent + ": " + dozenal(noCoinSpent);
	stats += "<br>" + strStatsWoodChopped + ": " + dozenal(noWoodChopped);
	stats += "<br>" + strStatsBuildingsErected + ": " + dozenal(noBuildingsErected);
	stats += "<br>" + strStatsQuestsCompleted + ": " + dozenal(noQuestsCompleted);
	stats += "<br>" + strStatsFlowersDelivered + ": " + dozenal(noFlowersFound);
	stats += "<br>" + strStatsJunkDelivered + ": " + dozenal(noJunkFound);
	stats += "<br>" + strStatsIncome + ": " + dozenal(coinAuto);
	stats += "<br>" + strStatsCasinoWinnings + ": " + dozenal(casinoNet);
	stats += "<br>" + strStatsLogSize + ": " + dozenal(logSize);
	stats += "<br>" + strStatsStyleLevel + ": " + dozenal(styleLevel);
	stats += "<br>" + strStatsMapLevel + ": " + dozenal(mapLevel);
	stats += "</div>";

	stats += "<br><br>" + strStatsInventory + "<div style='column-count:4'>";
	stats += strCoin + ": " + dozenal(coin);
	stats += "<br>" + strWood + ": " + dozenal(wood);
	stats += "<br>" + strFood + ": " + dozenal(food);
	if (gotAxe) stats += "<br>" + strItemAxe;
	if (gotClock) stats += "<br>" + strItemClock;
	if (gotCompass) stats += "<br>" + strItemCompass;
	if (gotKeyboard) stats += "<br>" + strItemKeyboard;
	if (gotSeedling) stats += "<br>" + strItemSeedling;
	if (junkCurrent) stats += "<br>" + strJunk + ": "+ junk[junkCurrent];
	if (flowerCurrent) stats += "<br>" + strFlower + ": "+ flowers[flowerActive];
	stats += "</div>";

	stats += "<br><br>" + strStatsEvents + "<div style='column-count:4'>";
	stats += strStatsTotal + ": " + dozenal(noEventsWonCatch + noEventsWonMath
		+ noEventsWonFight + noEventsWonPattern + noEventsLostCatch
		+ noEventsLostMath + noEventsLostFight + noEventsLostPattern);
	stats += " (" + dozenal(noEventsWonCatch + noEventsWonMath + noEventsWonFight + noEventsWonPattern);
	stats += " / " + dozenal(noEventsLostCatch + noEventsLostMath + noEventsLostFight + noEventsLostPattern) + ")";
	stats += "<br>" + strStatsCatch + ": " + dozenal(noEventsWonCatch + noEventsLostCatch) + " ("
		+ dozenal(noEventsWonCatch) + " / " + dozenal(noEventsLostCatch) + ")";
	stats += "<br>" + strStatsMath + ": " + dozenal(noEventsWonMath + noEventsLostMath) + " ("
		+ dozenal(noEventsWonMath) + " / " + dozenal(noEventsLostMath) + ")";
	stats += "<br>" + strStatsFight + ": " + dozenal(noEventsWonFight + noEventsLostFight) + " ("
		+ dozenal(noEventsWonFight) + " / " + dozenal(noEventsLostFight) + ")";
	stats += "<br>" + strStatsPattern + ": " + dozenal(noEventsWonPattern + noEventsLostPattern) + " ("
		+ dozenal(noEventsWonPattern) + " / " + dozenal(noEventsLostPattern) + ")";
	stats += "</div>";

	stats += "<br><br>" + strStatsWorld + "<div style='column-count:4'>";
	stats += strStatsUniverse + ": " + seed;
	stats += "<br>" + locationHome + ": " + locationHomes[map[mapCenter][mapCenter][2]];
	if (gotCompass) stats += "<br>" + strPosition + ": " + dozenal(position[0]) + "," + dozenal(position[1]);
	stats += "<br>" + strLevel + ": " + dozenal(level);
	stats += "<br>" + strEvent + ": " + dozenalPercent(eventChance);
	if (map[floristPosX][floristPosY][1]) stats += "<br>" + locationFlorist + ": " + dozenal(floristPosX) + "," + dozenal(floristPosY);
	if (map[junkingPosX][junkingPosY][1]) stats += "<br>" + locationJunking + ": " + dozenal(junkingPosX) + "," + dozenal(junkingPosY);
	if (onQuest) stats += "<br>" + strStatsOnQuest + towns[questTown][2];
	stats += "</div>";

	stats += "<br><br>" + strStatsTowns + "<div style='column-count:3'>";
	for (var i = 0; i < 3; i++) {
		if (map[towns[i][0]][towns[i][1]][1]) {
			stats += towns[i][2];
			stats += "<br>" + strPosition + ": " + dozenal(towns[i][0]) + "," + dozenal(towns[i][1]);
			stats += "<br>" + strRep + ": " + dozenal(towns[i][3]);
			stats += "<br>" + strStatsWorkers + ": " + dozenal(towns[i][4]);
			stats += "<br>" + strStatsIncome + ": " + dozenal(Math.floor(towns[i][3] / 12) * towns[i][4]);
			stats += "<br>" + strStatsQuests + ": " + dozenal(towns[i][5]) + "<br>";
		} else stats += strStatsUndiscovered + "<br><br><br><br><br>";
	}
	stats += "</div>";

	if (testing) {
		stats += "<br><br>TESTING<div style='column-count:4'>";
		stats += "version: " + version;
		stats += "<br>mapCenter: " + dozenal(mapCenter);
		stats += "<br>mapSize: " + dozenal(mapSize);
		stats += "<br>questDone: " + dozenal(questDone);
		stats += "<br>questType: " + questType;
		stats += "<br>questSavedCell: " + questSavedCell;
		stats += "<br>junkPos: " + dozenal(junkPosX) + "," + dozenal(junkPosY);
		stats += "<br>flowerPos: " + dozenal(flowerPosX) + "," + dozenal(flowerPosY);
		stats += "<br>currentTown: " + currentTown;
		stats += "<br>cell: " + cell;
		stats += "<br>Home upgrade level: " + dozenal(map[mapCenter][mapCenter][2]);
		stats += "<br>repRank: " + dozenal(repRank);
		stats += "<br>casinoBet0: " + dozenal(casinoBet0);
		stats += "<br>priceWood: " + dozenal(priceWood);
		stats += "<br>priceWoodBuy: " + dozenal(priceWoodBuy);
		stats += "<br>priceWoodSell: " + dozenal(priceWoodSell);
		stats += "<br>quickProgress: " + quickProgress;
		stats += "<br>smartProgress: " + smartProgress;
		stats += "<br>toughProgress: " + toughProgress;
		stats += "<br>godmode: " + godmode;
		stats += "<br>gameOver: " + gameOver;
		stats += "<br>coinGoal: " + dozenal(coinGoal);
		stats += "<br>mapColors: " + mapColors;
		stats += "<br>mapImages: " + mapImages;
		stats += "<br>viewSize: " + dozenal(viewSize);
		stats += "<br>radius: " + dozenal(radius);
		stats += "</div>";
	}

	statsAllEl.innerHTML = stats;
}

function toggle (panel) {
	var panelEl;

	incRep();

	switch (panel) {
		case "stats": panelEl = statsEl; break;
		case "log": panelEl = logEl; break;
		default: panelEl = mapEl; break;
	}

	if (panelToggled) {
		if (panel == "map") {
			mapminiEl.style.display = "block";
			mapfullEl.style.display = "none";
			displayMap();
		} else if (panel == "stats") {
			statsAllEl.style.display = "none";
			statsColumn1El.style.display = "";
			statsColumn2El.style.display = "";
		} else if (panel == "log") {
			styleLogEntrySize = 12;
			styleLogColumns = 1;
		}
		mapEl.style.display = "inline-block";
		statsEl.style.display = "inline-block";
		logEl.style.display = "inline-block";
		buttonsEl.style.display = "inline-block";
		panelEl.style.height = "50%";
		panelEl.style.width = "50%";
		panelToggled = 0;
	} else {
		if (panel == "map") {
			mapminiEl.style.display = "none";
			mapfullEl.style.display = "block";
			mapfullEl.style.top = "calc(50% - " + (mapfullEl.offsetHeight / 2) + "px)";
			displayMapFull();
		} else if (panel == "stats") {
			statsAllEl.style.display = "block";
			statsColumn1El.style.display = "none";
			statsColumn2El.style.display = "none";
			updateStatsAll();
		} else if (panel == "log") {
			styleLogEntrySize = 24;
			styleLogColumns = 2;
		}
		mapEl.style.display = "none";
		statsEl.style.display = "none";
		logEl.style.display = "none";
		buttonsEl.style.display = "none";
		panelEl.style.display = "inline-block";
		panelEl.style.height = "100%";
		panelEl.style.width = "100%";
		panelToggled = 1;
	}
	updateStyle();
}

function updateStyle () {
	var bgImage;
	var minimapHeight = mapminiEl.offsetHeight / 2;
	var logEntryHeight = logEl.offsetHeight / styleLogEntrySize;
	var minimapDivH = mapminiEl.offsetHeight / (viewSize * 2 + 1);
	var minimapDivW = mapminiEl.offsetWidth / (viewSize * 2 + 1);

	mapminiEl.style.top = "calc(50% - " + minimapHeight + "px)";

	styleEl.innerHTML = "";
	if (styleLevel > 1) {
		styleEl.innerHTML += "#map, #stats, #log, #buttons { border: solid 1px grey; }";
		styleEl.innerHTML += "#map:hover, #stats:hover, #log:hover, #buttons:hover { border-color: white; }";
	}

	if (styleLevel > 2) {
		styleEl.innerHTML += "#statQuick { color: forestgreen; }";
		styleEl.innerHTML += "#statSmart { color: cornflowerblue; }";
		styleEl.innerHTML += "#statTough { color: crimson; }";
	}

	if (styleLevel > 3) styleEl.innerHTML += "#log { background: midnightblue; }";
	if (styleLevel > 4) styleEl.innerHTML += "#log { background: darkslategrey; }";
	if (styleLevel > 5) styleEl.innerHTML += "#stats, #log, #buttons { font-family: sans-serif; }";

	switch (bgTheme) {
		case 1: bgImage = "bgPlain"; break;
		case 2: bgImage = "bgForest"; break;
		case 3: bgImage = "bgHill"; break;
		case 4: bgImage = "bgRiver"; break;
		default: bgImage = "bg"; break;
	}
	if (mapLevel > 3) mapEl.style.backgroundImage = "url('svg/" + bgImage + ".svg')";
	else mapEl.style.backgroundImage = "";

	//styleEl.innerHTML += ".minimapCell { height: " + minimapDivH + "px; width: " + minimapDivW + "px;}";
	styleEl.innerHTML += "#statsColumn1 div, #statsColumn2 div { height: " + (statsEl.offsetHeight / 12) + "px; line-height: " + (statsEl.offsetHeight / 12) + "px; }";
	styleEl.innerHTML += ".logEntry { height: " + logEntryHeight + "px; }";
	styleEl.innerHTML += "#log { columns: " + styleLogColumns + "; }";
	styleEl.innerHTML += "#buttons div { line-height: " + button1.offsetHeight + "px; font-size: x-large; }";
}
