var dozenalEl = document.getElementById("dozenal");
var styleEl = document.getElementById("style");
var mapEl = document.getElementById("map");
var logEl = document.getElementById("log");
var statsEl = document.getElementById("stats");
var buttonsEl = document.getElementById("buttons");
var eventEl = document.getElementById("events");
var mapminiEl = document.getElementById("mapmini");
var mapfullEl = document.getElementById("mapfull");
var statsColumn1El = document.getElementById("statsColumn1");
var statsColumn2El = document.getElementById("statsColumn2");
var repEl = document.getElementById("statRep");
var timeEl = document.getElementById("statTime");
var energyEl = document.getElementById("statEnergy");
var coinEl = document.getElementById("statCoin");
var woodEl = document.getElementById("statWood");
var foodEl = document.getElementById("statFood");
var quickEl = document.getElementById("statQuick");
var smartEl = document.getElementById("statSmart");
var toughEl = document.getElementById("statTough");
var statPositionEl = document.getElementById("statPosition");
var statLevelEl = document.getElementById("statLevel");
var statEventEl = document.getElementById("statEvent");
var locationEl = document.getElementById("statLocation");
var progressEl = document.getElementById("statProgress");
var specificEl = document.getElementById("statSpecific");
var statsAllEl = document.getElementById("statsAll");
var button1 = document.getElementById("button1");
var button2 = document.getElementById("button2");
var button3 = document.getElementById("button3");
var button4 = document.getElementById("button4");
var button5 = document.getElementById("button5");
var button6 = document.getElementById("button6");
var button7 = document.getElementById("button7");
var button8 = document.getElementById("button8");
var button9 = document.getElementById("button9");
var eventHeaderEl = document.getElementById("eventHeader");
var eventMsgEl = document.getElementById("eventMsg");
var eventPlayEl = document.getElementById("eventPlay");
var eventLeaveEl = document.getElementById("eventLeave");
var screen = document.getElementById("eventScreen");
var loadingPageEl = document.getElementById("loadingPage");

var svgPlain = "<img src='svg/plain.svg'>";
var svgForest = "<img src='svg/forest.svg'>";
var svgHill = "<img src='svg/hill.svg'>";
var svgRiver = "<img src='svg/river.svg'>";
var svgPlayer = "<img src='svg/player.svg'>";
var svgHome = "<img src='svg/home.svg'>";
var svgBorder = "<img src='svg/border.svg'>";
var svgBridge = "<img src='svg/bridge.svg'>";
var svgFarm = "<img src='svg/farm.svg'>";
var svgTown = "<img src='svg/town.svg'>";
var svgMarket = "<img src='svg/market.svg'>";
var svgCasino = "<img src='svg/casino.svg'>";
var svgTerminal = "<img src='svg/terminal.svg'>";
var svgGym = "<img src='svg/gym.svg'>";
var svgRange = "<img src='svg/range.svg'>";
var svgLibrary = "<img src='svg/library.svg'>";
var svgQuest = "<img src='svg/quest.svg'>";
var svgJunking = "<img src='svg/junking.svg'>";
var svgFlorist = "<img src='svg/florist.svg'>";

function makeMap () {
	for (var i = 0; i < mapSize; i++) {
		map[i] = [];

		for (var j = 0; j < mapSize; j++) {
			map[i][j] = [];
			map[i][j][0] = plain; // type
			map[i][j][1] = 0; // visibility
			map[i][j][2] = 0; // progress
			mapfullEl.innerHTML += "<div id='" + i + "+" + j + "'></div>";
		}

		mapfullEl.innerHTML += "<br>";
	}

	map[mapCenter][mapCenter][1] = 1;
}

function makePattern (x, y, cell) {
	var chance;
	var limitCheck = mapSize - 2;

	if (((x < limitCheck) && (x > 2)) && ((y < limitCheck) && (y > 2))) {
		map[x][y][0] = cell;
		map[x - 1][y][0] = cell;
		map[x + 1][y][0] = cell;
		map[x][y - 1][0] = cell;
		map[x][y + 1][0] = cell;

		chance = Math.floor(rand() * 6);
		switch (chance) {
			case 1:
				map[x + 1][y + 1][0] = cell;
				map[x + 1][y - 1][0] = cell;
				map[x - 1][y + 1][0] = cell;
				map[x - 1][y - 1][0] = cell;
				break;
			case 2:
				map[x + 1][y + 1][0] = cell;
				map[x + 1][y + 2][0] = cell;
				map[x - 1][y - 1][0] = cell;
				map[x - 1][y - 2][0] = cell;
				break;
			case 3:
				map[x + 1][y - 1][0] = cell;
				map[x + 1][y - 2][0] = cell;
				map[x - 1][y + 1][0] = cell;
				map[x - 1][y + 2][0] = cell;
				break;
			case 4:
				map[x + 1][y + 1][0] = cell;
				map[x + 2][y + 1][0] = cell;
				map[x - 1][y - 1][0] = cell;
				map[x - 2][y - 1][0] = cell;
				break;
			case 5:
				map[x + 1][y - 1][0] = cell;
				map[x + 2][y - 1][0] = cell;
				map[x - 1][y + 1][0] = cell;
				map[x - 2][y + 1][0] = cell;
				break;
			case 6:
				map[x + 2][y][0] = cell;
				map[x - 2][y][0] = cell;
				map[x][y - 2][0] = cell;
				map[x][y + 2][0] = cell;
				break;
		}
	}
}

function makeForests () {
	var x, y;

	makePattern(mapCenter, mapCenter, forest);

	for (var i = 0; i < 3; i++) {
		x = Math.floor(rand() * mapSize);
		y = Math.floor(rand() * mapSize);

		makePattern(x - 1, y, forest);
		makePattern(x + 1, y, forest);
		makePattern(x - 3, y, forest);
		makePattern(x + 3, y, forest);
		makePattern(x - 5, y, forest);
		makePattern(x + 5, y, forest);
		makePattern(x, y - 1, forest);
		makePattern(x, y + 1, forest);
		makePattern(x, y - 3, forest);
		makePattern(x, y + 3, forest);
		makePattern(x, y - 5, forest);
		makePattern(x, y + 5, forest);
	}
}

function makeHills () {
	var x, y;

	for (var i = 0; i < 3; i++) {
		x = randomInt(2, mapSize - 2);
		y = randomInt(2, mapSize - 2);
		makePattern(x, y, hill);
	}
}

function makeRiver () {
	var start = Math.floor(rand() * ((mapSize - 1) * 2)); // cannot start on 0,0
	var change;
	var limitCheck = mapSize - 3;
	var x = y = 0;
	var topStart = 0;

	var bridge1, bridge2, bridge3;
	bridge1 = mapCenter;
	bridge2 = randomInt(2, mapCenter - 2);
	bridge3 = randomInt(mapCenter + 2, mapSize - 3);

	if (start < mapSize) {
		x = start;
	} else { // start from top
		topStart = 1;
		y = start - mapSize;
	}

	for (var i = 0; i < mapSize; i++) {
		change = rand();
		makeBridge = randomInt(1,5);

		if (topStart == 1) {
			x = i;
			if ((change < 0.3) && (y > 2)) {
				y--;
			} else if ((change > 0.6) && (y < limitCheck)) {
				y++;
			}
		} else {
			y = i;
			if ((change < 0.3) && (x > 2)) {
				x--;
			} else if ((change > 0.6) && (x < limitCheck)) {
				x++;
			}
		}

		if ((i == bridge1) || (i == bridge2) || (i == bridge3)) {
			map[x][y][0] = bridge;
		} else {
			map[x][y][0] = river;
		}
	}
}

function erectBuilding (town, building) {
	var townX = towns[town][0];
	var townY = towns[town][1];
	var random = randomInt(1, 8);

	switch (random) {
		case 1: map[townX][townY - 1][0] = building; break;
		case 2: map[townX][townY + 1][0] = building; break;
		case 3: map[townX - 1][townY][0] = building; break;
		case 4: map[townX + 1][townY][0] = building; break;
		case 5: map[townX - 1][townY - 1][0] = building; break;
		case 6: map[townX - 1][townY + 1][0] = building; break;
		case 7: map[townX + 1][townY - 1][0] = building; break;
		case 8: map[townX + 1][townY + 1][0] = building; break;
	}
}

function makeTowns () {
	var ttc; // temporary town coordinates
	var rand;
	var tempNames = townNames.splice(0, townNames.length);

	rand = randomInt(0, tempNames.length-1)
	ttc = scatter(2, 3, town);
	towns[0] = [];
	towns[0][0] = ttc[0];
	towns[0][1] = ttc[1];
	towns[0][2] = tempNames[rand];
	towns[0][3] = 0; // rep
	towns[0][4] = 0; // workers
	towns[0][5] = 0; // quests
	tempNames.splice(rand, 1);

	rand = randomInt(0, tempNames.length-1)
	ttc = scatter(5, 7, town);
	towns[1] = [];
	towns[1][0] = ttc[0];
	towns[1][1] = ttc[1];
	towns[1][2] = tempNames[rand];
	towns[1][3] = 0;
	towns[1][4] = 0;
	towns[1][5] = 0;
	tempNames.splice(rand, 1);

	rand = randomInt(0, tempNames.length-1)
	ttc = scatter(9, 10, town);
	towns[2] = [];
	towns[2][0] = ttc[0];
	towns[2][1] = ttc[1];
	towns[2][2] = tempNames[rand];
	towns[2][3] = 0;
	towns[2][4] = 0;
	towns[2][5] = 0;
	tempNames.splice(rand, 1);

	erectBuilding(0, market);
	erectBuilding(1, casino);
	erectBuilding(2, terminal);

}

// place a letter in a [innerLimit, outerLimit] radius
function scatter (innerLimit, outerLimit, letter) {
	if (innerLimit > outerLimit) { alert(ErrorScatterFunction); }

	var i, j, k;
	var position = [];
	var randomPosition;
	var letterPlaced = 0;
	var noCandidateCells = 0;

	var currentRing = innerLimit;
	var innerStart = mapCenter - innerLimit;
	var innerEnd = mapCenter + innerLimit;
	var outerStart = mapCenter - outerLimit;
	var outerEnd = mapCenter + outerLimit;

	// find total number of candidate cells
	while (currentRing <= outerLimit) {
		noCandidateCells = noCandidateCells + 8 * currentRing; // 8 * currentRing counts cells in a ring
		currentRing++;
	}

	while (letterPlaced == 0) {
		randomPosition = Math.floor(rand() * noCandidateCells);
		k = 0;

		for (i = outerStart; i <= outerEnd; i++) {
			for (j = outerStart; j <= outerEnd; j++) {
				if (((j < innerEnd) && (j > innerStart)) && ((i < innerEnd) && (i > innerStart))) {
					// skip inner rings
				} else if (k == randomPosition) {
					if ((map[i][j][0] == plain) || (map[i][j][0] == forest)) {
						map[i][j][0] = letter;
						position[0] = i;
						position[1] = j;
						letterPlaced = 1;
					}
					i = outerEnd + 1;
					j = i;
				} else {
					k++; // skip (but count) cells inside valid rings
				}
			}
		}
	}

	return position;
}

function makeBorders () {
	for (var i = 0; i < mapSize; i++) {
		map[0][i][0] = border;
		map[i][0][0] = border;
		map[mapSize-1][i][0] = border;
		map[i][mapSize-1][0] = border;
	}
}

// rng seed https://stackoverflow.com/a/47593316/4432800
var seed, rand;
function xmur3(str) {
	for(var i = 0, h = 1779033703 ^ str.length; i < str.length; i++) {
		h = Math.imul(h ^ str.charCodeAt(i), 3432918353);
		h = h << 13 | h >>> 19;
	} return function() {
		h = Math.imul(h ^ (h >>> 16), 2246822507);
		h = Math.imul(h ^ (h >>> 13), 3266489909);
		return (h ^= h >>> 16) >>> 0;
	}
}
function mulberry32(a) {
	return function() {
		var t = a += 0x6D2B79F5;
		t = Math.imul(t ^ t >>> 15, t | 1);
		t ^= t + Math.imul(t ^ t >>> 7, t | 61);
		return ((t ^ t >>> 14) >>> 0) / 4294967296;
	}
}

function warmup () {
	statsEl.style.visibility = "";
	repEl.style.visibility = "";

	if (rep == 12) startGame();
	else button5.innerHTML = dozenal(12 - rep)
}

var audioPlain = new Audio('audio/plain1.ogg');
var audioForest = new Audio('audio/forest1.ogg');
function startGame () {
	setInterval(incTime,1000);
	displayMap();
	update();

	mapEl.style.visibility = "visible";
	logEl.style.visibility = "visible";
	locationEl.style.visibility = "visible";

	//~ audioPlain.loop = true;
	//~ audioForest.loop = true;
	//~ audioPlain.muted = true;
	//~ audioForest.muted = true;
	//~ audioPlain.play();
	//~ audioForest.play();
}

function init (initSeed) {
	seed = initSeed;
	if (seed == 0) seed = Math.floor(Math.random() * (999999999999 - 100000000000 + 1)) + 100000000000;
	seedInit = xmur3(seed.toString());
	rand = mulberry32(seedInit());

	makeMap();
	makeForests();
	makeHills();
	makeRiver();
	makeTowns();
	makeBorders();
	updateWoodPrices();

	var specialPlaced = false;
	while (!specialPlaced) specialPlaced = cellSpecialPlace(junking);
	junkPlace();

	specialPlaced = false;
	while (!specialPlaced) specialPlaced = cellSpecialPlace(florist);

	map[mapCenter][mapCenter][0] = home;
	cell = start;
	cellUpgradeLevel = map[position[0]][position[1]][2];

	for (var i = 0; i < mapSize; i++) {
		for (var j = 0; j < mapSize; j++) {
			if (map[i][j][0] == forest) forestPlace(i, j);
		}
	}

	repEl.style.visibility = "hidden";
	timeEl.style.visibility = "hidden";
	energyEl.style.visibility = "hidden";
	coinEl.style.visibility = "hidden";
	woodEl.style.visibility = "hidden";
	foodEl.style.visibility = "hidden";
	quickEl.style.visibility = "hidden";
	smartEl.style.visibility = "hidden";
	toughEl.style.visibility = "hidden";

	button1.style.visibility = "hidden";
	button2.style.visibility = "hidden";
	button3.style.visibility = "hidden";
	button4.style.visibility = "hidden";
	button6.style.visibility = "hidden";
	button7.style.visibility = "hidden";
	button8.style.visibility = "hidden";
	button9.style.visibility = "hidden";
	button5.innerHTML = "10";

	mapfullEl.style.display = "none";
	statsAllEl.style.display = "none";
	updateStyle();
	mapEl.style.visibility = "hidden";
	logEl.style.visibility = "hidden";
	statsEl.style.visibility = "hidden";

	screen.style.display = "block";
	eventPlayEl.innerHTML = eventPlay;
	eventLeaveEl.innerHTML = eventRun;

	dozenalEl.addEventListener("resize", function () { updateStyle(); });
	mapEl.addEventListener("click", function () { toggle(this.id); });
	logEl.addEventListener("click", function () { toggle(this.id); });
	statsEl.addEventListener("click", function () { toggle(this.id); });
	eventEl.addEventListener("click", function () { incRep(); });
	button1.addEventListener("click", function () { klik(1); });
	button2.addEventListener("click", function () { klik(2); });
	button3.addEventListener("click", function () { klik(3); });
	button4.addEventListener("click", function () { klik(4); });
	button5.addEventListener("click", function () { klik(5); });
	button6.addEventListener("click", function () { klik(6); });
	button7.addEventListener("click", function () { klik(7); });
	button8.addEventListener("click", function () { klik(8); });
	button9.addEventListener("click", function () { klik(9); });

	if (testing) {
		loadingPageEl.innerHTML = "v:" + version + " seed:" + seed;
		loadingPageEl.style.display = "inline";
		loadingPageEl.style.position = "fixed";
		loadingPageEl.style.top = "initial";
		loadingPageEl.style.left = 0;
		loadingPageEl.style.bottom = 0;
		loadingPageEl.style.height = "1em";
		loadingPageEl.style.width = "initial";
		loadingPageEl.style.lineHeight = "1em";
	} else loadingPageEl.remove();
}
