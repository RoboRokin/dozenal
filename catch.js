function catchRunners () {
	var runnerSpeed;
	var runner;

	catchRunnersList = [];
	catchRunnersEscapes = [];
	catchNoCatched = 0;
	catchRunnerMoving = 0;

	catchNoRunners = level + 1;
	runnerSpeed = 1000 * quick / level;
	if (isQuest == 1) catchNoRunners += Math.floor(catchNoRunners / 12);

	for (var i = 0; i < catchNoRunners; i++) {
		runner = document.createElement("button");
		runner.className = "runner";
		runner.setAttribute("id", "runnerNo" + i);
		runner.onclick = function() { runnerCatched(this.id); };
		catchRunnersList.push(runner);
		screen.appendChild(runner);
		catchRunnersEscapes[i] = 10;
		runnerSingleMove(i);
	}

	runnersMove();
	catchInterval = setInterval(runnersMove, runnerSpeed);
}

function runnerSingleMove (index) {
	var percent;

	catchRunnersList[index].style.top = Math.floor(rand() * (eventScreenSizeY - 60)) + "px";
	catchRunnersList[index].style.left = Math.floor(rand() * (eventScreenSizeX - 60)) + "px";

	percent = catchRunnersEscapes[index] / 10 * 100;
	catchRunnersList[index].innerHTML = dozenal(catchRunnersEscapes[index]);
	catchRunnersList[index].style.backgroundImage = "linear-gradient(to top, green " + percent + "%, white " + percent + "%)";
}

function runnersMove () {
	var k = 0;
	var random;
	var percent;

	if (catchRunnersList[catchRunnerMoving]) {
		catchRunnersEscapes[catchRunnerMoving]--;
		runnerSingleMove(catchRunnerMoving);
	}

	if (catchRunnersEscapes[catchRunnerMoving] < 1) catchEnd(0);
	if (++catchRunnerMoving >= catchNoRunners) catchRunnerMoving = 0;
}

function runnerCatched (name) {
	var tmp = name.slice(8);

	catchNoCatched++;
	document.getElementById(name).style.background = "gray";
	document.getElementById(name).onclick = "";
	catchRunnersList[tmp] = null;
	if (catchNoCatched == catchNoRunners) catchEnd(1);
}

function catchEnd (win) {
	for (var i = 0; i < catchNoRunners; i++) {
		document.getElementById("runnerNo" + i).onclick = "";
	}

	if (win) eventMsgEl.innerHTML = "You catched them all!";
	else eventMsgEl.innerHTML = "One escaped!";

	clearInterval(catchInterval);
	endEvent(win);
}
