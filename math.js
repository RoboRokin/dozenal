function solveMath () {
	var newButton;
	var a = b = c = d = 0;
	var maxNumber = level * 12;
	var questionEl = document.createElement("div");

	if (smart == 144) mathNoAnswers = 1;
	else {
		mathNoAnswers = level - Math.floor(smart / 12);
		if (isQuest) mathNoAnswers += 1;
		if (mathNoAnswers > 12) mathNoAnswers = 12;
		else if (mathNoAnswers < 2) mathNoAnswers = 2;
	}

	mathCorrectAnswer = Math.floor(rand() * mathNoAnswers);

	if (level < 4) {
		a = randomInt(1, maxNumber);
		b = randomInt(1, maxNumber);
	} else if (level < 7) {
		a = randomInt(1, maxNumber);
		b = randomInt(1, maxNumber);
		c = randomInt(1, maxNumber);
	} else if (level < 10) {
		a = randomInt(-maxNumber, maxNumber);
		b = randomInt(-maxNumber, maxNumber);
	} else {
		a = randomInt(-maxNumber, maxNumber);
		b = randomInt(-maxNumber, maxNumber);
		c = randomInt(-maxNumber, maxNumber);
	}

	d = a + b + c;
	screen.appendChild(questionEl);
	questionEl.className = "question";
	questionEl.style.lineHeight = questionEl.offsetHeight + "px";

	if (c == 0) {
		questionEl.innerHTML = dozenal(a) + checkPrefix(dozenal(b)) + " = ?";
	} else {
		questionEl.innerHTML = dozenal(a) + checkPrefix(dozenal(b)) + checkPrefix(dozenal(c)) + " = ?";
	}

	for (var i = 0; i < mathNoAnswers; i++) {
		newButton = document.createElement("div");
		newButton.className = "answer";
		newButton.setAttribute("id", "answerNo" + i);
		if (i == mathCorrectAnswer) {
			newButton.innerHTML = dozenal(d);
			newButton.onclick = function() { solveMathEnd(1, this.id); };
		} else {
			newButton.innerHTML = dozenal(d + findRandomAddition());
			newButton.onclick = function() { solveMathEnd(0, this.id); };
		}

		screen.appendChild(newButton);
		newButton.style.width = "calc(100% / 3)";
		newButton.style.height = "calc(100% / 5)";
		newButton.style.lineHeight = newButton.offsetHeight + "px";
	}
}

function findRandomAddition () {
	var number = 0;
	while (number == 0) number = Math.floor(rand() * 20) - 10;
	return number;
}

function checkPrefix (number) {
	if (number.charAt(0) == "-") return number;
	else return "+" + number;
}

function solveMathEnd (win, clicked) {
	for (var i = 0; i < mathNoAnswers; i++) {
		document.getElementById("answerNo" + i).onclick = "";
		document.getElementById("answerNo" + i).className += " answerGreyed";
		if (i == mathCorrectAnswer) document.getElementById("answerNo" + i).style.background = "green";
	}

	if (win) eventMsgEl.innerHTML = strCorrect;
	else {
		eventMsgEl.innerHTML = strWrong;
		document.getElementById(clicked).style.background = "red";
	}

	endEvent(win);
}
