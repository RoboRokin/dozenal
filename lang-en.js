// ALERTS
var ErrorLocalStorage = "Local storage not working";
var ErrorScatterFunction = "Scatter function error";

// CELLS
var start = "0";
var player = "*";
var border = "X";
var home = "H";
var plain = ".";
var forest = "#";
var hill = "^";
var river = "~";
var bridge = "=";
var farm = "v";
var town = "T";
var market = "M";
var casino = "C";
var terminal = "B";
var gym = "G";
var range = "8";
var library = "L";
var bandits = "P";
var dungeon = "Q";
var puzzles = "S";
var quest = "!";
var junking = "J";
var florist = "F";

var strRep = "Rep";
var strTime = "Exp";
var strEnergy = "Energy";
var strCoin = "Coin";
var strWood = "Wood";
var strFood = "Food";
var strQuick = "Quick";
var strSmart = "Smart";
var strTough = "Tough";
var strPosition = "Position";
var strLevel = "Level";
var strEvent = "Event";
var strJunk = "Junk";
var strFlower = "Flower";
var strLvl = " lvl ";
var strNothing = "nothing";

var strStats = "STATS"
var strStatsInventory = "INVENTORY"
var strStatsEvents = "EVENTS"
var strStatsWorld = "WORLD"
var strStatsTowns = "TOWNS"
var strStatsGameOver = "Game completed! Time: ";
var strStatsMoves = "Moves";
var strStatsEnergyUsed = "Energy used";
var strStatsCoinEarned = "Coin earned";
var strStatsCoinSpent = "Coin spent";
var strStatsWoodChopped = "Wood chopped";
var strStatsBuildingsErected = "Buildings erected";
var strStatsQuestsCompleted = "Quests completed";
var strStatsFlowersDelivered = "Flowers delivered";
var strStatsJunkDelivered = "Junk delivered";
var strStatsIncome = "Income";
var strStatsCasinoWinnings = "Casino winnings";
var strStatsLogSize = "Log size";
var strStatsStyleLevel = "Style level";
var strStatsMapLevel = "Map level";
var strStatsTotal = "Total (w/l)";
var strStatsCatch = "Catch";
var strStatsMath = "Math";
var strStatsFight = "Fight";
var strStatsPattern = "Pattern";
var strStatsUniverse = "Universe";
var strStatsWorkers = "Workers";
var strStatsQuests = "Quests";
var strStatsOnQuest = "On quest for: ";
var strStatsUndiscovered = "(undiscovered)";

var symbolCoin = "C";
var symbolWood = "W";
var symbolFood = "F";

var locationHome = "Home";
var locationPlains = "Plains";
var locationForest = "Forest";
var locationHill = "Hill";
var locationRiver = "River";
var locationBridge = "Bridge";
var locationFarm = "Farm";
var locationTown = "Town";
var locationMarket = "Market";
var locationCasino = "Casino";
var locationTerminal = "Terminal";
var locationGym = "Gym";
var locationRange = "Range";
var locationLibrary = "Library";
var locationBandits = "Bandit camp";
var locationDungeon = "Dungeon";
var locationMystery = "Mystery";
var locationQuest = "Quest";
var locationJunking = "Junking";
var locationFlorist = "Florist";

var strLocUpgrade = "Upgrade";
var strLocErectCost = "Erect cost";
var strLocFarmCost = "Farm cost";
var strLocTownRep = "Town rep";
var strLocIncome = "Income";
var strLocWorkers = "Workers";
var strLocQuests = "Quests";
var strLocHireCost = "Hire cost";
var strLocOnQuest = "ON QUEST";
var strLocWoodBuy = "Wood buy";
var strLocWoodSell = "Wood sell";
var strLocMapUpgrade = "Map upgrade";
var strLocStyleUpgrade = "Style upgrade";
var strLocCurrentBet = "Current bet";
var strLocNetWinnings = "Net winnings";
var strLocTravelCost = "Travel cost";
var strLocUpgradeCost = "Upgrade cost";
var strLocTrainerCost = "Trainer cost";
var strLocJunkDelivered = "Junk delivered";
var strLocFlowersDelivered = "Flowers delivered";

var strItemAxe = "axe";
var strItemClock = "clock";
var strItemCompass = "compass";
var strItemKeyboard = "keyboard";
var strItemSeedling = "seedling";

var actionMoveUp = "&#9650;";
var actionMoveDown = "&#9660;";
var actionMoveLeft = "&#9664;";
var actionMoveRight = "&#9654;";
var actionRest = "rest";
var actionChop = "chop";
var actionWork = "work";
var actionHire = "hire";
var actionSleep = "sleep";
var actionSave = "save";
var actionLoad = "load";
var actionUpgrade = "upgrade";
var actionTrain = "train";
var actionLift = "lift";
var actionSkate = "skate";
var actionStudy = "study";
var actionGamble = "gamble";
var actionFight = "FIGHT";
var actionLoot = "LOOT";
var actionScience = "SCIENCE";
var actionQuest = "quest";
var actionErect = "erect";
var actionPlant = "plant";
var actionFarm = "farm";
var actionShop = "shop";
var actionEvent = "event";
var actionQuit = "quit";
var actionTrade = "TRADE";
var actionShop = "shop";
var actionMap = "map";
var actionStyle = "style";
var actionSell = "sell";
var actionBuy = "buy";

var logMoveUp = "You travel north";
var logMoveDown = "You travel south";
var logMoveLeft = "You travel west";
var logMoveRight = "You travel east";
var needCoin = "Insufficient coin";
var gameSaved = "You went to sleep";
var gameLoaded = "You woke up";
var homeSlept = "You feel refreshed";
var strGameOver = "GAME OVER in ";

var upgradedMemory = "You can remember more";
var upgradedStyle = "You are more stylish";
var upgradedMap = "Your map is improved";
var upgradedQuick = "You are faster";
var upgradedSmart = "You are smarter";
var upgradedTough = "You are stronger";
var upgradedGym = "You upgraded Gym";
var upgradedRange = "You upgraded Range";
var upgradedLibrary = "You upgraded Library";
var upgradedTerminal = "You upgraded Terminal";
var strWorked = "You work hard for ";
var strChopped = "You clear some forest and get ";
var strErected = "You erected a ";

var foundBandits = "There is a bandit camp nearby";
var foundDungeon = "You can smell a dungeon nearby";
var foundMystery = "A mystery is close";

var textGambleWin = "Coin won: ";
var textGambleLose = "Coin lost: ";

var strBuyAxe = "You buy an axe to chop wood";
var strBuyClock = "You buy a clock to tell the time";
var strBuyCompass = "You buy a compass to know your location";
var strBuyKeyboard = "You buy a keyboard for easier movement";
var strBuySeedling = "You buy a seedling to plant a forest";
var strMarketBuyW = "You buy 1W for ";
var strMarketSellW = "You sell 1W for ";

var eventRun = "Run";
var eventPlay = "Play";
var eventLeave = "Leave";
var eventStart = "Event started";
var eventFailed = "You failed!";
var eventRunResult = "You ran and dropped ";
var eventWinResult = "You succeeded and earned ";
var eventPowStart = "Punch the enemy!";
var eventQuiStart = "Catch the mouse!";
var eventSmaStart = "Find the letter!";
var eventMatStart = "Don't get cheated!;"
var eventIntroGeneric = "Stuff is happening!";

var strCorrect = "Correct!";
var strWrong = "Wrong!";
var strFightWon = "You won the fight!";
var strFightLost = "You lost the fight!";

var questFail = "You are too weak for this quest";
var questWinP = "You win by punching!";
var questWinQ = "Got the little bastard!";
var questWinS = "You win at letters!";
var questWinT = "You earned some coin";
var questQuit = "You quit the quest";

var locationHomes = ["Hole", "Box", "Dwelling", "Shack", "Lodge", "Cottage",
	"House", "Mansion", "Villa", "Castle", "Palace", "Xanadu"];

var repTitles = [
	"god",
	"sleeper",
	"noob",
	"worker",
	"prospect",
	"adventurer",
	"king",
	"winner" ];

var repRankPromotions = [
	"You are insane",
	"unknown",
	"unknown",
	"You are a worker",
	"You are a prospect",
	"You are an adventurer",
	"You are a king",
	"You are a winner" ];

var townNames = [
	"Cityville",
	"Townsburg",
	"Autumnfield",
	"Hillstone",
	"Woodsdam",
	"Farview" ];

var restNotes = [
	"Your rest a bit",
	"You take a short nap",
	"The view is beautiful",
	"Birds want to have sex",
	"You found nothing",
	"You lost track of time",
	"You are homesick",
	"There is a cricket riding a tumbleweed",
	"It looks like a nice day",
	"The scar hurts",
	"That plastic bag is beautiful",
	"It is nice to be alone",
	"Gust of wind goes by",
	"Your stomach growls",
	"You blaze one up",
	"Loneliness feels bittersweet",
	"There is no one around",
	"Wolf howls in the distance" ];

var homeIntros = [
	"A Hole in the ground looks good enough",
	"Put a roof so you have your own Box",
	"Some furniture turns your Box into a Dwelling",
	"You've built yourself a nice little Shack",
	"Shack was really ugly, so you refurnished it into a Lodge",
	"Cottage has a lot of room for your coin",
	"Easy living with a sweet House",
	"Easier living with an even sweeter Mansion",
	"Showing everyone how much coin you have with the new Villa",
	"You erected a huge Castle deserving of your accomplishments",
	"Placed a Palace for maximum comfort",
	"Ego to the max with your own Xanadu" ];

var junk = ["none", "round rock", "broken wheel", "empty bottle", "paper money",
	"rat skull", "toothless cog", "single sock", "dull blade",
	"crumpled newspaper", "barbed wire", "old stick", "curly string"];

var junkingFoundJunkUndiscovered = "You found some junk but don't know what to do with it";
var junkingFoundJunk = "You found more junk for Junking";
var junkingResponses = [
	"Junking asks you to bring him any junk you find",
	"Junking likes round rocks",
	"Junking feels for all things broken",
	"Junking thinks he can fill that empty bottle",
	"Junking teaches you can write on any paper",
	"Junking collects skulls, even rat ones",
	"Junking carves out some tooths on that cog",
	"Junking says that two single socks make a pair",
	"Junking will use the dull blade to spread butter",
	"Junking throws crumpled newspaper around as a ball",
	"Junking rakes the leaves with barbed wire",
	"Junking gives the old stick a name",
	"Junking uses curly string as decoration" ];

var flowers = [
	"Cinderbell",
	"Farania",
	"Redbell",
	"Stinkweed",
	"Darkmaris",
	"Dawnflower",
	"Waterwilly",
	"Duskflower",
	"Willy",
	"Greenbell",
	"Tillydrop",
	"Rugamilla" ];

var floristResponse = "Florist thanks you for the flower";
var floristFoundFlower = "You found the flower for the florist";
var floristRequests = [
	"Florist wants a Cinderbell, sometimes grows in town gardens",
	"Florist wants a Farania, can be found in the hills",
	"Florist wants a Redbell, often grows in the north",
	"Florist wants a Stinkweed, which can grow around buildings",
	"Florist wants a Darkmaris, common in the forest",
	"Florist wants a Dawnflower, grows far in the east",
	"Florist wants a Waterwilly, only grows underwater",
	"Florist wants a Duskflower, usually grows west",
	"Florist wants a Willy flower, which grows on plains",
	"Florist wants a Greenbell, sometimes can be found south",
	"Florist wants a Tillydrop, usually grows alongside rivers",
	"Florist wants a Rugamilla, an exceptionally rare flower" ];

var introMessageFight = [
	"Punch the enemy!",
	"You are being attack by bandits",
	"Someone challenges you to a fight",
	"Some goats are defending their territory"];

var introMessageCatch = [
	"Catch the mice!",
	"Rat infestation!",
	"Squirrels are stealing your food",
	"Whack a mole!"];

var introMessageMath = [
	"Don't get cheated!",
	"You meet your old teacher",
	"Someone is testing your brain",
	"Math is useful"];

var introMessagePattern = [
	"Find the letters!",
	"Complete the pattern",
	"The place is very crowded",
	"Simon says"];

var randomQuests = [
	"Town asks you for help",
	"You are helping the town",
	"You helped the town",
	"Town is grateful for your help" ];

var questsQuick = [];
questsQuick [0] = [
	"Mayor asks you to catch some worms",
	"You find some worms near your home",
	"You catch all the worms",
	"Mayor is a bit disgusted but thanks you" ];

questsQuick [1] = [
	"Mayor asks you to deal with some rats",
	"You find the rats mayor was talking about",
	"You deal with all the rats",
	"Mayor is relieved and rewards you" ];

questsQuick [2] = [
	"Mayor wishes you distribute some food",
	"There are many hungry people",
	"You successfully distributed the food",
	"Mayor rewards you for your service" ];

questsQuick [3] = [
	"Mayor wants you to exterminate some snakes",
	"There is a bunch of snakes",
	"You managed to get all the snakes",
	"Mayor is pleased there are no more snakes" ];

questsQuick [4] = [
	"Mayor requests you go handle a hornets nest",
	"There is a whole swarm of hornets here",
	"You remove every single hornet",
	"Mayor is in awe that you removed the hornets" ];

questsQuick [5] = [
	"Mayor thinks you can remove ants that are infesting an area",
	"Ants are trying to overwhelm you",
	"You overwhelmed the ants",
	"Mayor is very thankful for removing the ants" ];

var questsSmart = [];
questsSmart [0] = [
	"Council asks you for some basic information",
	"You are trying to find the information at home",
	"You managed to find the necessary information",
	"Council welcomes you to the town" ];

questsSmart [1] = [
	"Council requests some data crunching",
	"You are rifling through the data",
	"The numbers are crunched",
	"Council is satisfied with your work" ];

questsSmart [2] = [
	"Council thinks you can fix some irregularities",
	"There is a bunch of irregularities",
	"You manage to sort all irregularities",
	"Council is pleased they were right about you" ];

questsSmart [3] = [
	"Council allows you to fill the paperwork for joining",
	"The paperwork is enormous",
	"You complete all required paperwork",
	"Council takes your request into consideration" ];

questsSmart [4] = [
	"Council tasks you with making a census before joining",
	"You are trying to find all people in the region",
	"You completed the census",
	"Council is grateful for your service" ];

questsSmart [5] = [
	"Council asks you archive all your work to join",
	"You had a lot of work",
	"You sorted and archived your work",
	"Council approves you request and grants you a seat" ];

var questsTough = [];
questsTough [0] = [
	"Sheriff wants you to prove you can handle yourself",
	"You find some weak bandits around your home",
	"You take trophies of the bandits to show the sheriff",
	"Sheriff is impressed with your work" ];

questsTough [1] = [
	"Sheriff wants you to register as their bounty hunter",
	"There is a lot of paperwork",
	"Papers are no match for you",
	"Sheriff congratulates you on becoming an official bounty hunter" ];

questsTough [2] = [
	"Sheriff wants you to dispose of some nasty bandits in the wild",
	"Bandits tried to ambush you",
	"Their ambush was a failure",
	"Sheriff rewards you for a work well done" ];

questsTough [3] = [
	"Sheriff offers a bounty for a notorious group of bandits",
	"You ambush the bandits",
	"Bandits had no chance",
	"Sheriff is very pleased you are good at your work" ];

questsTough [4] = [
	"Sheriff thinks you can single-handedly deal with the bandit fort",
	"There are bandits hiding in the fort",
	"You managed to deal with all bandits",
	"Sheriff was not sure you would return, and is glad you did" ];

questsTough [5] = [
	"Sheriff thinks you are ready to eliminate bandit leader",
	"The bandit leader is huge",
	"The bandit leader is no more",
	"Sheriff thanks you for everything you have done" ];
