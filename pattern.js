var patternNoAnswers; // 6-144
var patternCurrentSymbol;
var patternQuestion = [];
var patternSymbols = [35, 42, 43, 61, 64, 162, 163, 164, 165, 167, 169, 174, 177,
	181, 198, 208, 215, 222, 248, 254, 338, 8225, 8226, 8364];

function solvePattern () {
	var noSymbols; // 2-12
	var symbols = patternSymbols.slice(0);;
	var questionEl = document.createElement("div");

	noSymbols = level;
	if (noSymbols < 2) noSymbols = 2;

	patternNoAnswers = level * level - Math.floor((quick + smart + tough) / 3);
	if (patternNoAnswers < 6) patternNoAnswers = 6;
	if (patternNoAnswers < noSymbols) patternNoAnswers = noSymbols;

	patternCurrentSymbol = 0;
	patternQuestion = [];

	screen.appendChild(questionEl);
	questionEl.className = "question";
	questionEl.style.lineHeight = questionEl.offsetHeight + "px";

	var random;
	for (var i = 0; i < noSymbols; i++) {
		random = randomInt(0, symbols.length - 1);
		patternQuestion[i] = symbols[random];
		symbols.splice(random, 1);
		questionEl.innerHTML += "<span id='pattern" + i + "'>" + String.fromCharCode(patternQuestion[i]) + "</span>";
	}

	var questionShuffledTemp = [];
	var questionShuffled = [];
	questionShuffledTemp = patternQuestion.slice(0);
	for (var i = 0; i < patternQuestion.length; i++) {
		questionShuffled[i] = questionShuffledTemp.splice(randomInt(0, questionShuffledTemp.length-1), 1);
	}

	var possibleCorrectAnswers = [];
	for (var i = 0; i < patternNoAnswers; i++) {
		possibleCorrectAnswers[i] = i;
	}
	var actualCorrectAnswers = [];
	for (var i = 0; i < noSymbols; i++) {
		random = randomInt(0, possibleCorrectAnswers.length - 1);
		actualCorrectAnswers[i] = possibleCorrectAnswers[random];
		possibleCorrectAnswers.splice(random, 1);
	}
	actualCorrectAnswers.sort(function(a, b) { return a - b });

	var currentSymbol = 0;
	var answerEl;
	var answersEl = document.createElement("div");
	answersEl.style.width = "100%";
	answersEl.style.height = "80%";
	screen.appendChild(answersEl);
	var patternAnswerHeight = answersEl.offsetHeight / Math.ceil(patternNoAnswers / 12);
	for(var i = 0; i < patternNoAnswers; i++) {
		answerEl = document.createElement("div");
		answerEl.setAttribute("id", "answerNo" + i);
		answerEl.className = "answer answerPattern";
		answerEl.style.height = patternAnswerHeight + "px";
		answerEl.style.lineHeight = patternAnswerHeight + "px";
		if (i == actualCorrectAnswers[currentSymbol]) {
			answerEl.innerHTML = String.fromCharCode(questionShuffled[currentSymbol]);
			answerEl.onclick = function() { solvePatternCorrect(this); };
			currentSymbol++;
		} else {
			answerEl.innerHTML = String.fromCharCode(symbols[randomInt(0, symbols.length - 1)]);
			answerEl.onclick = function() { solvePatternEnd(0); };
		}
		answersEl.appendChild(answerEl);
	}
}

function solvePatternCorrect (button) {
	if (button.innerHTML == String.fromCharCode(patternQuestion[patternCurrentSymbol])) {
		document.getElementById("pattern" + patternCurrentSymbol).style.background = "green";
		patternCurrentSymbol++;
		button.style.visibility = "hidden";
		if (patternCurrentSymbol == patternQuestion.length) {
			solvePatternEnd(1);
		}
	} else { solvePatternEnd(0); }
}

function solvePatternEnd (win) {
	for (var i = 0; i < patternNoAnswers; i++) {
		document.getElementById("answerNo" + i).onclick = "";
		document.getElementById("answerNo" + i).className += " answerGreyed";
	}

	if (win) eventMsgEl.innerHTML = strCorrect;
	else eventMsgEl.innerHTML = strWrong;

	endEvent(win);
}
